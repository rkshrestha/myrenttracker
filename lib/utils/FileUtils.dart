import 'dart:io';

import 'package:my_rent_tracker/utils/Log.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class FileUtils {

 // static String PROPERTY_DIR="renttracket/images/property";
  static String PROPERTY_DIR="renttracker";


 static  Future<String> get localPath async {
    final directory = await getExternalStorageDirectory();
    return directory.path;
  }

  static Future<File>  createFile(String dirPath,String fileName) async {
    final path = await localPath;
    Log.v("file saved" + path.toString());
    String propertyDirPath = join(path,dirPath);
    Directory dir = Directory(propertyDirPath);
    bool isDirExist = await dir.exists();
    Directory newDir;
    if (!isDirExist) {
      newDir = await dir.create();
    } else {
      newDir = dir;
    }
    return File('${newDir.path}/$fileName');
  }


  static Future<Directory>  getDir(String dirPath) async {
    final path = await localPath;
    Log.v("Directory saved" + path.toString());

    String propertyDirPath = join(path, dirPath);
    Log.v("Directory saved" + propertyDirPath.toString());
    Directory dir = Directory(propertyDirPath);
    bool isDirExist = await dir.exists();
    Directory newDir;
    if (!isDirExist) {
      newDir = await dir.create();
    }

    return newDir;
  }

static void saveFile(File selectedFile,String filename) async{
  File savedFile1= await createFile(PROPERTY_DIR,filename);
  savedFile1.writeAsBytes(selectedFile.readAsBytesSync(),mode:FileMode.write);
}



}
