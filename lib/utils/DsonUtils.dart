import 'dart:convert';

import 'package:chopper/chopper.dart';
import 'package:my_rent_tracker/features/repo/JsonCallable.dart';

class DsonUtils{

   static T toObject<T>(String json,JsonCallable callable) {
     Map map = jsonDecode(json);
    return callable.fromJson(map) as T;
  }

  static String toJsonString(Object object){
     return jsonEncode(object);
  }

//   static String toJsonStringWithDynamic<T>(Object object,JsonCallable callable){
//     String data = jsonEncode(object);
//     return jsonEncode(toObject(data, callable));
//   }


  static List<T> toList<T>(String json,JsonCallable callable) {
    List dataList = jsonDecode(json);
    List<T> newList=[];
    for(int i=0;i<dataList.length;i++){
      T organization = callable.fromJson(dataList[i]);
      newList.add(organization);
    }
    return newList;
  }

  static List<T> dynamicToList<T>(dynamic json,JsonCallable callable) {
    String data = jsonEncode(json);
    List dataList = jsonDecode(data);
    List<T> newList=[];
    for(int i=0;i<dataList.length;i++){
      T organization = callable.fromJson(dataList[i]);
      newList.add(organization);
    }
    return newList;
  }

//  public static HashMap<String, String> getHashMap(Object src) {
//    String data = toString(src);
//    Timber.v( data);
//    return toObject(data, new TypeToken<HashMap<String, String>>() {
//    }.getType());
//  }
}