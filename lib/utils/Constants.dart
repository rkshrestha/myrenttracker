class Constants{
  static const String Info = 'Info';
  static const String SetPin = 'Set Pin Code';
  static const String Change = 'Change pin code';
  static const String LogOut ='Log Out';

  static const List<String> choices = <String>[
    Info,
    SetPin,

  ];

  static const List<String> choiceAfterSetup = <String>[
    Info,
    Change,
    LogOut,
  ];


}