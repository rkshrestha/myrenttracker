import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/splash/SplashPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Rent Tracker',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.green,
        primaryColorDark: Colors.green,
      ),
      home: SplashPage(),
    );
  }
}
