import 'dart:async';

import 'package:my_rent_tracker/features/bloc/BaseBloc.dart';
import 'package:my_rent_tracker/features/network/SCResponse.dart';
import 'package:my_rent_tracker/features/repo/Property.dart';
import 'package:my_rent_tracker/features/repo/PropertyRepo.dart';

class PropertyBloc extends BaseBloc {
  PropertyRepo propertyRepo;

  StreamController<SCResponse<List<Property>>> scPropertyList =
      StreamController.broadcast();

  StreamController<SCResponse<String>> scSaveProperty =
      StreamController.broadcast();

  StreamController<SCResponse<String>> scDeleteProperty =
      StreamController.broadcast();

  StreamController<SCResponse<String>> scUpdateProperty =
      StreamController.broadcast();

  PropertyBloc(this.propertyRepo);

  Stream<SCResponse<List<Property>>> getPropertyList() {
    this.propertyRepo.getPropertyList(scPropertyList.sink);
    return scPropertyList.stream;
  }

  Stream<SCResponse<String>> saveProperty(Property property) {
    this.propertyRepo.saveProperty(property, scSaveProperty.sink);
    return scSaveProperty.stream;
  }

  Stream<SCResponse<String>> deleteProperty(Property property) {
    this.propertyRepo.deleteProperty(property, scDeleteProperty.sink);
    return scDeleteProperty.stream;
  }

  Stream<SCResponse<String>> updateProperty(Property property) {
    this.propertyRepo.updateProperty(property, scUpdateProperty.sink);
    return scUpdateProperty.stream;
  }

  @override
  void onDispose() {
    scPropertyList.close();
    scSaveProperty.close();
    scDeleteProperty.close();
    scUpdateProperty..close();
  }
}
