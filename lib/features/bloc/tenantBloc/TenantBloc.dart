import 'dart:async';

import 'package:my_rent_tracker/features/bloc/BaseBloc.dart';
import 'package:my_rent_tracker/features/network/SCResponse.dart';
import 'package:my_rent_tracker/features/repo/Tenant.dart';
import 'package:my_rent_tracker/features/repo/TenantRepo.dart';

class TenantBloc extends BaseBloc {
  TenantRepo tenantRepo;

  StreamController<SCResponse<List<Tenant>>> scTenantList =
      StreamController.broadcast();

  StreamController<SCResponse<String>> scSaveTenant =
      StreamController.broadcast();


  StreamController<SCResponse<String>> scDeleteTenant =
  StreamController.broadcast();

  TenantBloc(this.tenantRepo);


  Stream<SCResponse<List<Tenant>>> getTenantList() {
    this.tenantRepo.getTenantList(scTenantList.sink);
    return scTenantList.stream;
  }

  Stream<SCResponse<String>> saveTenant(Tenant tenant) {
    this.tenantRepo.saveTenant(tenant, scSaveTenant.sink);
    return scSaveTenant.stream;
  }

  Stream<SCResponse<String>> deleteTenant(Tenant tenant) {
    this.tenantRepo.deleteTenant(tenant, scDeleteTenant.sink);
    return scDeleteTenant.stream;
  }

  @override
  void onDispose() {
    scTenantList.close();
    scSaveTenant.close();
    scDeleteTenant.close();
  }
}
