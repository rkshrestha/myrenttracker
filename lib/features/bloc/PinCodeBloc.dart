
import 'dart:async';

import 'package:my_rent_tracker/features/bloc/BaseBloc.dart';
import 'package:my_rent_tracker/features/network/SCResponse.dart';
import 'package:my_rent_tracker/features/repo/PinCode.dart';
import 'package:my_rent_tracker/features/repo/PinCodeRepo.dart';

class PinCodeBloc extends BaseBloc{
  PinCodeRepo pinCodeRepo;

  StreamController<SCResponse<String>> scSavePinCode =
  StreamController.broadcast();


  PinCodeBloc(this.pinCodeRepo);

  Stream<SCResponse<String>> savePinCode(PinCode pinCode) {
    this.pinCodeRepo.savePinCode(pinCode, scSavePinCode.sink);
    return scSavePinCode.stream;
  }

  @override
  void onDispose() {
    scSavePinCode.close();
  }
}