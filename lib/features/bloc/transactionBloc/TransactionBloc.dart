import 'dart:async';

import 'package:my_rent_tracker/features/bloc/BaseBloc.dart';
import 'package:my_rent_tracker/features/network/SCResponse.dart';
import 'package:my_rent_tracker/features/repo/Transaction.dart';
import 'package:my_rent_tracker/features/repo/TransactionRepo.dart';

class TransactionBloc extends BaseBloc {
  TransactionRepo transactionRepo;
  StreamController<SCResponse<List<Transaction>>> scTransactionList =
  StreamController.broadcast();

  StreamController<SCResponse<String>> scSaveTransaction =
  StreamController.broadcast();

  StreamController<SCResponse<String>> scDeleteTransaction =
  StreamController.broadcast();


  TransactionBloc(this.transactionRepo);

  Stream<SCResponse<List<Transaction>>> getTransactionList() {
    this.transactionRepo.getTransactionList(scTransactionList.sink);
    return scTransactionList.stream;
  }

  Stream<SCResponse<String>> saveTransaction(Transaction transaction) {
    this.transactionRepo.saveTransaction(transaction, scSaveTransaction.sink);
    return scSaveTransaction.stream;
  }

  Stream<SCResponse<String>> deleteTransaction (Transaction transaction){
    this.transactionRepo.deleteTransaction(transaction, scDeleteTransaction.sink);
    return scDeleteTransaction.stream;
  }

  @override
  void onDispose() {
    scTransactionList.close();
    scSaveTransaction.close();
    scDeleteTransaction.close();

  }
}
