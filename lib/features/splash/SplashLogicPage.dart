import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/home/TransactionPageList.dart';
import 'package:my_rent_tracker/features/home/PropertyPageList.dart';
import 'package:my_rent_tracker/features/home/TenantPageList.dart';
import 'package:my_rent_tracker/features/splash/SplashPage.dart';
import 'package:permission/permission.dart';

class SplashLogicPage {
  SplashPageState splashState;

  SplashLogicPage(this.splashState);

  void onPostState() {
    Future.delayed(Duration(milliseconds: 3000)).then((_) {
      checkPermission();
    });
  }

  void gotoHomePage() {
    Navigator.pushReplacement(
      this.splashState.context,
      MaterialPageRoute(builder: (context) => PropertyPageList()),
    );
  }

  void requestPermission() async {
    List<Permissions> permissions = await Permission.requestPermissions([
      PermissionName.Contacts,
      PermissionName.SMS,
      PermissionName.Camera,
      PermissionName.Phone,
      PermissionName.Storage,
    ]);
    if (!isPermissionDenied(permissions)) {
      gotoHomePage();
    } else {
      requestPermission();
    }
  }

  bool isPermissionDenied(List<Permissions> permissions) {
    bool permissionDeny = false;
    permissions.forEach((permission) {
      if (permission.permissionStatus == PermissionStatus.deny) {
        permissionDeny = true;
      }
    });
    return permissionDeny;
  }

  void checkPermission() async {
    List<Permissions> permissions = await Permission.getPermissionsStatus([
      PermissionName.Contacts,
      PermissionName.SMS,
      PermissionName.Camera,
      PermissionName.Phone,
      PermissionName.Storage,
    ]);
    if (!isPermissionDenied(permissions)) {
      gotoHomePage();
    } else {
      requestPermission();
    }
  }
}
