import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/splash/SplashLogicPage.dart';

class SplashPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashPageState();
  }
}

class SplashPageState extends State<SplashPage> {
  SplashLogicPage splashLogic;

  @override
  void initState() {
    super.initState();
    splashLogic = SplashLogicPage(this);
    splashLogic.onPostState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: Image.asset(
                  'assets/images/homeImage.png',
                  fit: BoxFit.contain,
                  width: 100.0,
                  height: 100.0,
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 10.0)),
              Text(
                "My Rent Tracker",
                style: TextStyle(
                    color: Colors.green,
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold),
              )
            ],
          ),
        ],
      ),
    );
  }
}
