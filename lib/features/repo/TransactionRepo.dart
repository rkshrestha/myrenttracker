import 'dart:async';

import 'package:my_rent_tracker/features/network/SCResponse.dart';
import 'package:my_rent_tracker/features/repo/Repository.dart';
import 'package:my_rent_tracker/features/repo/SQFliteDatabase.dart';
import 'package:my_rent_tracker/features/repo/Transaction.dart';
import 'package:my_rent_tracker/features/repo/TransactionDao.dart';
import 'package:my_rent_tracker/utils/Log.dart';

class TransactionRepo implements Repository<Transaction> {
  SQFliteDatabase sqFliteDatabase;
  TransactionDao transactionDao;
  List<Transaction> transactionList;

  TransactionRepo() {
    Log.v("inside TransactionRepo");
    init();
  }

  init() async {
    sqFliteDatabase = SQFliteDatabase.getDB();
  }

  void getTransactionList(
      StreamSink<SCResponse<List<Transaction>>> stSink) async {
    try {
      transactionDao = await sqFliteDatabase.transactionDao();

      transactionList = await transactionDao.findAll();

      for (Transaction transaction in transactionList) {
        Log.v(transaction.transactionId.toString() +
            "," +
            transaction.monthlyRent);
      }

      stSink.add(SCResponse.success(transactionList));
    } catch (ex) {
      Log.v(ex.toString());
    }
  }

  void saveTransaction(
      Transaction transaction, StreamSink<SCResponse<String>> sSink) async {
    try {
      transactionDao = await sqFliteDatabase.transactionDao();

      int transactionId = await transactionDao.persist(Transaction.make(
          transaction.date,
          transaction.monthlyRent,
          transaction.electricity,
          transaction.water,
          transaction.sanitation,
          transaction.dueAmount,
          transaction.paymentDate,
          transaction.paid));

      sSink.add(
          SCResponse.success("Transaction has been successfully inserted"));
    } catch (ex) {
      sSink.add(SCResponse.error("Couldn't insert transaction"));
      Log.v(ex.toString());
    }
  }

  deleteTransaction(
      Transaction transaction, StreamSink<SCResponse<String>> sSink) async {
    transactionDao = await sqFliteDatabase.transactionDao();
    try {
      await transactionDao.delete(transaction);
      sSink.add(SCResponse.success("Transaction deleted successfully"));
    } catch (ex) {
      sSink.add(SCResponse.error("Couldn't delete transaction"));
      Log.v(ex.toString());
    }
  }

  @override
  void delete(Transaction entity) {
    // TODO: implement delete
  }

  @override
  void deleteAll() {
    // TODO: implement deleteAll
  }

  @override
  List<Transaction> findAll() {
    // TODO: implement findAll
    return null;
  }

  @override
  Transaction findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  Transaction findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  void persist(Transaction entity) {
    // TODO: implement persist
  }

  @override
  void update(Transaction entity) {
    // TODO: implement update
  }
}
