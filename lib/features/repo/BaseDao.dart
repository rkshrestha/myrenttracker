class BaseDao<T>{

  Future<void> persist(T entity){}


  Future<void>  update(T entity){}


  Future<T> findById(Object id){}

  Future<T> findOne(){}


  Future<void> delete(T entity){}


  Future<List<T>> findAll(){}


  Future<void> deleteAll(){}

}