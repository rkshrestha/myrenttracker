import 'dart:async';

import 'package:my_rent_tracker/features/network/SCResponse.dart';
import 'package:my_rent_tracker/features/repo/Property.dart';
import 'package:my_rent_tracker/features/repo/PropertyDao.dart';
import 'package:my_rent_tracker/features/repo/Repository.dart';
import 'package:my_rent_tracker/features/repo/SQFliteDatabase.dart';
import 'package:my_rent_tracker/utils/Log.dart';

class PropertyRepo implements Repository<Property> {
  SQFliteDatabase sqFliteDatabase;
  PropertyDao propertyDao;
  List<Property> propertyList;

 // static PropertyRepo _instance;

/*  PropertyRepo._(){
    init();
  }

  static PropertyRepo getInstance(){
    if(_instance==null){
      _instance=PropertyRepo._();
    }
    return _instance;
  }*/

  PropertyRepo() {
    Log.v("inside propertyRepo");
    init();
  }

  init() async {
    sqFliteDatabase = SQFliteDatabase.getDB();
  }

  void getPropertyList(StreamSink<SCResponse<List<Property>>> sSink) async {
    try {
      propertyDao = await sqFliteDatabase.propertyDao();

      propertyList = await propertyDao.findAll();

      for (Property property in propertyList) {
        Log.v(property.propertyId.toString() + "," + property.name);
      }

      sSink.add(SCResponse.success(propertyList));
    } catch (ex) {

      Log.v(ex.toString());

    }
  }

//

  void saveProperty(Property property,
      StreamSink<SCResponse<String>> sSink) async {
    try {
      propertyDao = await sqFliteDatabase.propertyDao();

      Log.v('check property add ');

      int propertyId = await propertyDao.persist(new Property.make(
          property.name, property.description, property.imagePath));

      sSink.add(SCResponse.success("Property has been successfully inserted"));
    } catch (ex) {
      sSink.add(SCResponse.error("Couldn't insert property"));
      Log.v(ex.toString());
    }
  }

  deleteProperty(Property property,
      StreamSink<SCResponse<String>> sSink) async {
    try {
      propertyDao = await sqFliteDatabase.propertyDao();

      await propertyDao.delete(property);

      sSink.add(SCResponse.success("Property deleted Successfully"));
    } catch (ex) {
      sSink.add(SCResponse.error("Couldn't delete property"));
      Log.v(ex.toString());
    }
  }


  updateProperty(Property property,
      StreamSink<SCResponse<String>> sSink) async {
    try {
      propertyDao = await sqFliteDatabase.propertyDao();

      await propertyDao.update(property);

      sSink.add(SCResponse.success("Property updated Successfully"));
    } catch (ex) {
      sSink.add(SCResponse.error("Couldn't update property"));
      Log.v(ex.toString());
    }
  }


@override
void delete(Property entity) {
  // TODO: implement delete
}

@override
void deleteAll() {
  // TODO: implement deleteAll
}

@override
List<Property> findAll() {
  // TODO: implement findAll
  return null;
}

@override
Property findById(Object id) {
  // TODO: implement findById
  return null;
}

@override
Property findOne() {
  // TODO: implement findOne
  return null;
}

@override
void persist(Property entity) {
  // TODO: implement persist
}

@override
void update(Property entity) {
  // TODO: implement update
}}
