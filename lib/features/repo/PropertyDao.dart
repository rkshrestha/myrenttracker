import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:my_rent_tracker/features/repo/BaseDao.dart';
import 'package:my_rent_tracker/features/repo/Property.dart';
import 'package:my_rent_tracker/utils/Log.dart';

class PropertyDao implements BaseDao<Property> {
  static PropertyDao _instance;
  PropertyBean bean;

//  PropertyDao._();
//
//  static PropertyDao get(PropertyBean beann){
//    if(_instance==null){
//      Log.v("propertyDao bean");
//      bean=beann;
//      _instance=PropertyDao._();
//    }
//    return _instance;
//  }

  PropertyDao._(this.bean);

  static PropertyDao get(PropertyBean bean) {
    if (_instance == null) {
      _instance = PropertyDao._(bean);
    }
    return _instance;
  }

  @override
  Future<void> delete(Property property) {
    Log.v('propertyId:${property.propertyId}');
    return bean.remove(property.propertyId);
  }

  @override
  Future<void> deleteAll() {
    // TODO: implement deleteAll
    return null;
  }

  @override
  Future<List<Property>> findAll() {
    Find findProperty = Find(bean.tableName);
    return bean.findMany(findProperty);
  }

  @override
  Future<Property> findById(Object id) async {
    return bean.find(id);
  }

  @override
  Future<Property> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<int> persist(Property property) async {
    await bean.createTable(ifNotExists: true);
    Log.v("create Table property");
    return await bean.insert(property);
  }

  @override
  Future<int> update(Property property) {
    return bean.update(property);
  }
}
