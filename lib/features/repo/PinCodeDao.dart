
import 'package:my_rent_tracker/features/repo/BaseDao.dart';
import 'package:my_rent_tracker/features/repo/PinCode.dart';
import 'package:my_rent_tracker/utils/Log.dart';

class PinCodeDao implements BaseDao<PinCode>{
  static PinCodeDao _instance;
  PinCodeBean bean;

  PinCodeDao._(this.bean);

  static PinCodeDao get(PinCodeBean bean) {
    if (_instance == null) {
      _instance = PinCodeDao._(bean);
    }
    return _instance;
  }



  @override
  Future<void> delete(PinCode entity) {
    // TODO: implement delete
    return null;
  }

  @override
  Future<void> deleteAll() {
    // TODO: implement deleteAll
    return null;
  }

  @override
  Future<List<PinCode>> findAll() {
    // TODO: implement findAll
    return null;
  }

  @override
  Future<PinCode> findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  Future<PinCode> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<int> persist(PinCode pinCode)async {
    await bean.createTable(ifNotExists: true);
    Log.v("create Table pincode");
    return await bean.insert(pinCode);
  }

  @override
  Future<void> update(PinCode entity) {
    // TODO: implement update
    return null;
  }

}