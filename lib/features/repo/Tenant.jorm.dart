// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Tenant.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _TenantBean implements Bean<Tenant> {
  final tenantId = IntField('tenant_id');
  final mobileNumber = StrField('mobile_number');
  final fullName = StrField('full_name');
  final address = StrField('address');
  final occupancy = StrField('occupancy');
  final emailAddress = StrField('email_address');
  final leaseStartDate = StrField('lease_start_date');
  final leaseEndDate = StrField('lease_end_date');
  final monthlyValue = StrField('monthly_value');
  final dayValue = StrField('day_value');
  final paymentType = StrField('payment_type');
  final monthlyRent = StrField('monthly_rent');
  final electricity = StrField('electricity');
  final water = StrField('water');
  final sanitation = StrField('sanitation');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        tenantId.name: tenantId,
        mobileNumber.name: mobileNumber,
        fullName.name: fullName,
        address.name: address,
        occupancy.name: occupancy,
        emailAddress.name: emailAddress,
        leaseStartDate.name: leaseStartDate,
        leaseEndDate.name: leaseEndDate,
        monthlyValue.name: monthlyValue,
        dayValue.name: dayValue,
        paymentType.name: paymentType,
        monthlyRent.name: monthlyRent,
        electricity.name: electricity,
        water.name: water,
        sanitation.name: sanitation,
      };
  Tenant fromMap(Map map) {
    Tenant model = Tenant();
    model.tenantId = adapter.parseValue(map['tenant_id']);
    model.mobileNumber = adapter.parseValue(map['mobile_number']);
    model.fullName = adapter.parseValue(map['full_name']);
    model.address = adapter.parseValue(map['address']);
    model.occupancy = adapter.parseValue(map['occupancy']);
    model.emailAddress = adapter.parseValue(map['email_address']);
    model.leaseStartDate = adapter.parseValue(map['lease_start_date']);
    model.leaseEndDate = adapter.parseValue(map['lease_end_date']);
    model.monthlyValue = adapter.parseValue(map['monthly_value']);
    model.dayValue = adapter.parseValue(map['day_value']);
    model.paymentType = adapter.parseValue(map['payment_type']);
    model.monthlyRent = adapter.parseValue(map['monthly_rent']);
    model.electricity = adapter.parseValue(map['electricity']);
    model.water = adapter.parseValue(map['water']);
    model.sanitation = adapter.parseValue(map['sanitation']);

    return model;
  }

  List<SetColumn> toSetColumns(Tenant model,
      {bool update = false, Set<String> only, bool onlyNonNull: false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      if (model.tenantId != null) {
        ret.add(tenantId.set(model.tenantId));
      }
      ret.add(mobileNumber.set(model.mobileNumber));
      ret.add(fullName.set(model.fullName));
      ret.add(address.set(model.address));
      ret.add(occupancy.set(model.occupancy));
      ret.add(emailAddress.set(model.emailAddress));
      ret.add(leaseStartDate.set(model.leaseStartDate));
      ret.add(leaseEndDate.set(model.leaseEndDate));
      ret.add(monthlyValue.set(model.monthlyValue));
      ret.add(dayValue.set(model.dayValue));
      ret.add(paymentType.set(model.paymentType));
      ret.add(monthlyRent.set(model.monthlyRent));
      ret.add(electricity.set(model.electricity));
      ret.add(water.set(model.water));
      ret.add(sanitation.set(model.sanitation));
    } else if (only != null) {
      if (model.tenantId != null) {
        if (only.contains(tenantId.name)) ret.add(tenantId.set(model.tenantId));
      }
      if (only.contains(mobileNumber.name))
        ret.add(mobileNumber.set(model.mobileNumber));
      if (only.contains(fullName.name)) ret.add(fullName.set(model.fullName));
      if (only.contains(address.name)) ret.add(address.set(model.address));
      if (only.contains(occupancy.name))
        ret.add(occupancy.set(model.occupancy));
      if (only.contains(emailAddress.name))
        ret.add(emailAddress.set(model.emailAddress));
      if (only.contains(leaseStartDate.name))
        ret.add(leaseStartDate.set(model.leaseStartDate));
      if (only.contains(leaseEndDate.name))
        ret.add(leaseEndDate.set(model.leaseEndDate));
      if (only.contains(monthlyValue.name))
        ret.add(monthlyValue.set(model.monthlyValue));
      if (only.contains(dayValue.name)) ret.add(dayValue.set(model.dayValue));
      if (only.contains(paymentType.name))
        ret.add(paymentType.set(model.paymentType));
      if (only.contains(monthlyRent.name))
        ret.add(monthlyRent.set(model.monthlyRent));
      if (only.contains(electricity.name))
        ret.add(electricity.set(model.electricity));
      if (only.contains(water.name)) ret.add(water.set(model.water));
      if (only.contains(sanitation.name))
        ret.add(sanitation.set(model.sanitation));
    } else /* if (onlyNonNull) */ {
      if (model.tenantId != null) {
        ret.add(tenantId.set(model.tenantId));
      }
      if (model.mobileNumber != null) {
        ret.add(mobileNumber.set(model.mobileNumber));
      }
      if (model.fullName != null) {
        ret.add(fullName.set(model.fullName));
      }
      if (model.address != null) {
        ret.add(address.set(model.address));
      }
      if (model.occupancy != null) {
        ret.add(occupancy.set(model.occupancy));
      }
      if (model.emailAddress != null) {
        ret.add(emailAddress.set(model.emailAddress));
      }
      if (model.leaseStartDate != null) {
        ret.add(leaseStartDate.set(model.leaseStartDate));
      }
      if (model.leaseEndDate != null) {
        ret.add(leaseEndDate.set(model.leaseEndDate));
      }
      if (model.monthlyValue != null) {
        ret.add(monthlyValue.set(model.monthlyValue));
      }
      if (model.dayValue != null) {
        ret.add(dayValue.set(model.dayValue));
      }
      if (model.paymentType != null) {
        ret.add(paymentType.set(model.paymentType));
      }
      if (model.monthlyRent != null) {
        ret.add(monthlyRent.set(model.monthlyRent));
      }
      if (model.electricity != null) {
        ret.add(electricity.set(model.electricity));
      }
      if (model.water != null) {
        ret.add(water.set(model.water));
      }
      if (model.sanitation != null) {
        ret.add(sanitation.set(model.sanitation));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists: false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(tenantId.name,
        primary: true, autoIncrement: true, isNullable: false);
    st.addStr(mobileNumber.name, isNullable: true);
    st.addStr(fullName.name, isNullable: true);
    st.addStr(address.name, isNullable: true);
    st.addStr(occupancy.name, isNullable: true);
    st.addStr(emailAddress.name, isNullable: true);
    st.addStr(leaseStartDate.name, isNullable: true);
    st.addStr(leaseEndDate.name, isNullable: true);
    st.addStr(monthlyValue.name, isNullable: true);
    st.addStr(dayValue.name, isNullable: true);
    st.addStr(paymentType.name, isNullable: true);
    st.addStr(monthlyRent.name, isNullable: true);
    st.addStr(electricity.name, isNullable: true);
    st.addStr(water.name, isNullable: true);
    st.addStr(sanitation.name, isNullable: true);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(Tenant model,
      {bool cascade: false, bool onlyNonNull: false, Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(tenantId.name);
    var retId = await adapter.insert(insert);
    if (cascade) {
      Tenant newModel;
    }
    return retId;
  }

  Future<void> insertMany(List<Tenant> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(Tenant model,
      {bool cascade: false, Set<String> only, bool onlyNonNull: false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(tenantId.name);
    var retId = await adapter.upsert(upsert);
    if (cascade) {
      Tenant newModel;
    }
    return retId;
  }

  Future<void> upsertMany(List<Tenant> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(Tenant model,
      {bool cascade: false,
      bool associate: false,
      Set<String> only,
      bool onlyNonNull: false}) async {
    final Update update = updater
        .where(this.tenantId.eq(model.tenantId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<Tenant> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.tenantId.eq(model.tenantId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<Tenant> find(int tenantId,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.tenantId.eq(tenantId));
    return await findOne(find);
  }

  Future<int> remove(int tenantId) async {
    final Remove remove = remover.where(this.tenantId.eq(tenantId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<Tenant> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.tenantId.eq(model.tenantId));
    }
    return adapter.remove(remove);
  }
}
