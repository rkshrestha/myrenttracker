
import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:my_rent_tracker/features/repo/BaseDao.dart';
import 'package:my_rent_tracker/features/repo/Transaction.dart';
import 'package:my_rent_tracker/utils/Log.dart';

class TransactionDao implements BaseDao<Transaction> {
  static TransactionDao _instance;
  TransactionBean transactionBean;

  TransactionDao._(this.transactionBean);

  static TransactionDao get(TransactionBean bean) {
    if (_instance == null) {
      _instance = TransactionDao._(bean);
    }
    return _instance;
  }

  @override
  Future<void> delete(Transaction transaction) {
    Log.v('transactionId:${transaction.transactionId}');
    return transactionBean.remove(transaction.transactionId);
  }

  @override
  Future<void> deleteAll() {
    // TODO: implement deleteAll
    return null;
  }

  @override
  Future<List<Transaction>> findAll() {
    Find findTransaction = Find(transactionBean.tableName);
    return transactionBean.findMany(findTransaction);
  }

  @override
  Future<Transaction> findById(Object id) {
    return transactionBean.find(id);
  }

  @override
  Future<Transaction> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<int> persist(Transaction transaction) async {
    await transactionBean.createTable(ifNotExists: true);
    Log.v("create Table Transaction");
    return await transactionBean.insert(transaction);
  }

  @override
  Future<void> update(Transaction entity) {
    // TODO: implement update
    return null;
  }
}
