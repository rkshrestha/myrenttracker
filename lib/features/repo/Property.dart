import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:jaguar_query/jaguar_query.dart';

part 'Property.jorm.dart';

class Property {
  @PrimaryKey(auto: true)
  int propertyId;

  @Column(isNullable: true)
  String name;

  @Column(isNullable: true)
  String description;

  @Column(isNullable: true)
  String imagePath;

  Property();

  Property.make(this.name, this.description, this.imagePath);
}

@GenBean()
class PropertyBean extends Bean<Property> with _PropertyBean {
  PropertyBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'property';
}
