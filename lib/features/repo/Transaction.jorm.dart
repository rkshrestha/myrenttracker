// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Transaction.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _TransactionBean implements Bean<Transaction> {
  final transactionId = IntField('transaction_id');
  final date = StrField('date');
  final monthlyRent = StrField('monthly_rent');
  final electricity = StrField('electricity');
  final water = StrField('water');
  final sanitation = StrField('sanitation');
  final dueAmount = StrField('due_amount');
  final paymentDate = StrField('payment_date');
  final paid = StrField('paid');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        transactionId.name: transactionId,
        date.name: date,
        monthlyRent.name: monthlyRent,
        electricity.name: electricity,
        water.name: water,
        sanitation.name: sanitation,
        dueAmount.name: dueAmount,
        paymentDate.name: paymentDate,
        paid.name: paid,
      };
  Transaction fromMap(Map map) {
    Transaction model = Transaction();
    model.transactionId = adapter.parseValue(map['transaction_id']);
    model.date = adapter.parseValue(map['date']);
    model.monthlyRent = adapter.parseValue(map['monthly_rent']);
    model.electricity = adapter.parseValue(map['electricity']);
    model.water = adapter.parseValue(map['water']);
    model.sanitation = adapter.parseValue(map['sanitation']);
    model.dueAmount = adapter.parseValue(map['due_amount']);
    model.paymentDate = adapter.parseValue(map['payment_date']);
    model.paid = adapter.parseValue(map['paid']);

    return model;
  }

  List<SetColumn> toSetColumns(Transaction model,
      {bool update = false, Set<String> only, bool onlyNonNull: false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      if (model.transactionId != null) {
        ret.add(transactionId.set(model.transactionId));
      }
      ret.add(date.set(model.date));
      ret.add(monthlyRent.set(model.monthlyRent));
      ret.add(electricity.set(model.electricity));
      ret.add(water.set(model.water));
      ret.add(sanitation.set(model.sanitation));
      ret.add(dueAmount.set(model.dueAmount));
      ret.add(paymentDate.set(model.paymentDate));
      ret.add(paid.set(model.paid));
    } else if (only != null) {
      if (model.transactionId != null) {
        if (only.contains(transactionId.name))
          ret.add(transactionId.set(model.transactionId));
      }
      if (only.contains(date.name)) ret.add(date.set(model.date));
      if (only.contains(monthlyRent.name))
        ret.add(monthlyRent.set(model.monthlyRent));
      if (only.contains(electricity.name))
        ret.add(electricity.set(model.electricity));
      if (only.contains(water.name)) ret.add(water.set(model.water));
      if (only.contains(sanitation.name))
        ret.add(sanitation.set(model.sanitation));
      if (only.contains(dueAmount.name))
        ret.add(dueAmount.set(model.dueAmount));
      if (only.contains(paymentDate.name))
        ret.add(paymentDate.set(model.paymentDate));
      if (only.contains(paid.name)) ret.add(paid.set(model.paid));
    } else /* if (onlyNonNull) */ {
      if (model.transactionId != null) {
        ret.add(transactionId.set(model.transactionId));
      }
      if (model.date != null) {
        ret.add(date.set(model.date));
      }
      if (model.monthlyRent != null) {
        ret.add(monthlyRent.set(model.monthlyRent));
      }
      if (model.electricity != null) {
        ret.add(electricity.set(model.electricity));
      }
      if (model.water != null) {
        ret.add(water.set(model.water));
      }
      if (model.sanitation != null) {
        ret.add(sanitation.set(model.sanitation));
      }
      if (model.dueAmount != null) {
        ret.add(dueAmount.set(model.dueAmount));
      }
      if (model.paymentDate != null) {
        ret.add(paymentDate.set(model.paymentDate));
      }
      if (model.paid != null) {
        ret.add(paid.set(model.paid));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists: false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(transactionId.name,
        primary: true, autoIncrement: true, isNullable: false);
    st.addStr(date.name, isNullable: true);
    st.addStr(monthlyRent.name, isNullable: true);
    st.addStr(electricity.name, isNullable: true);
    st.addStr(water.name, isNullable: true);
    st.addStr(sanitation.name, isNullable: true);
    st.addStr(dueAmount.name, isNullable: true);
    st.addStr(paymentDate.name, isNullable: true);
    st.addStr(paid.name, isNullable: true);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(Transaction model,
      {bool cascade: false, bool onlyNonNull: false, Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(transactionId.name);
    var retId = await adapter.insert(insert);
    if (cascade) {
      Transaction newModel;
    }
    return retId;
  }

  Future<void> insertMany(List<Transaction> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(Transaction model,
      {bool cascade: false, Set<String> only, bool onlyNonNull: false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(transactionId.name);
    var retId = await adapter.upsert(upsert);
    if (cascade) {
      Transaction newModel;
    }
    return retId;
  }

  Future<void> upsertMany(List<Transaction> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(Transaction model,
      {bool cascade: false,
      bool associate: false,
      Set<String> only,
      bool onlyNonNull: false}) async {
    final Update update = updater
        .where(this.transactionId.eq(model.transactionId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<Transaction> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.transactionId.eq(model.transactionId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<Transaction> find(int transactionId,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.transactionId.eq(transactionId));
    return await findOne(find);
  }

  Future<int> remove(int transactionId) async {
    final Remove remove = remover.where(this.transactionId.eq(transactionId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<Transaction> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.transactionId.eq(model.transactionId));
    }
    return adapter.remove(remove);
  }
}
