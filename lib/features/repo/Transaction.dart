import 'package:jaguar_orm/jaguar_orm.dart';

part 'Transaction.jorm.dart';

class Transaction {
  @PrimaryKey(auto: true)
  int transactionId;

  @Column(isNullable: true)
  String date;

  @Column(isNullable: true)
  String monthlyRent;

  @Column(isNullable: true)
  String electricity;

  @Column(isNullable: true)
  String water;

  @Column(isNullable: true)
  String sanitation;

  @Column(isNullable: true)
  String dueAmount;

  @Column(isNullable: true)
  String paymentDate;

  @Column(isNullable: true)
  String paid;

  Transaction();

  Transaction.make(this.date, this.monthlyRent, this.electricity, this.water,
      this.sanitation, this.dueAmount, this.paymentDate, this.paid);
}

@GenBean()
class TransactionBean extends Bean<Transaction> with _TransactionBean {
  TransactionBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'transactions';

}