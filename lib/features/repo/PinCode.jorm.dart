// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'PinCode.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _PinCodeBean implements Bean<PinCode> {
  final pinCodeId = IntField('pin_code_id');
  final pinCode = StrField('pin_code');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        pinCodeId.name: pinCodeId,
        pinCode.name: pinCode,
      };
  PinCode fromMap(Map map) {
    PinCode model = PinCode();
    model.pinCodeId = adapter.parseValue(map['pin_code_id']);
    model.pinCode = adapter.parseValue(map['pin_code']);

    return model;
  }

  List<SetColumn> toSetColumns(PinCode model,
      {bool update = false, Set<String> only, bool onlyNonNull: false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      if (model.pinCodeId != null) {
        ret.add(pinCodeId.set(model.pinCodeId));
      }
      ret.add(pinCode.set(model.pinCode));
    } else if (only != null) {
      if (model.pinCodeId != null) {
        if (only.contains(pinCodeId.name))
          ret.add(pinCodeId.set(model.pinCodeId));
      }
      if (only.contains(pinCode.name)) ret.add(pinCode.set(model.pinCode));
    } else /* if (onlyNonNull) */ {
      if (model.pinCodeId != null) {
        ret.add(pinCodeId.set(model.pinCodeId));
      }
      if (model.pinCode != null) {
        ret.add(pinCode.set(model.pinCode));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists: false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(pinCodeId.name,
        primary: true, autoIncrement: true, isNullable: false);
    st.addStr(pinCode.name, isNullable: true);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(PinCode model,
      {bool cascade: false, bool onlyNonNull: false, Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(pinCodeId.name);
    var retId = await adapter.insert(insert);
    if (cascade) {
      PinCode newModel;
    }
    return retId;
  }

  Future<void> insertMany(List<PinCode> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(PinCode model,
      {bool cascade: false, Set<String> only, bool onlyNonNull: false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(pinCodeId.name);
    var retId = await adapter.upsert(upsert);
    if (cascade) {
      PinCode newModel;
    }
    return retId;
  }

  Future<void> upsertMany(List<PinCode> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(PinCode model,
      {bool cascade: false,
      bool associate: false,
      Set<String> only,
      bool onlyNonNull: false}) async {
    final Update update = updater
        .where(this.pinCodeId.eq(model.pinCodeId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<PinCode> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.pinCodeId.eq(model.pinCodeId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<PinCode> find(int pinCodeId,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.pinCodeId.eq(pinCodeId));
    return await findOne(find);
  }

  Future<int> remove(int pinCodeId) async {
    final Remove remove = remover.where(this.pinCodeId.eq(pinCodeId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<PinCode> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.pinCodeId.eq(model.pinCodeId));
    }
    return adapter.remove(remove);
  }
}
