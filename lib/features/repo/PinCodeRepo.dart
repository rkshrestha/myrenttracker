import 'dart:async';

import 'package:my_rent_tracker/features/network/SCResponse.dart';
import 'package:my_rent_tracker/features/repo/PinCode.dart';
import 'package:my_rent_tracker/features/repo/PinCodeDao.dart';
import 'package:my_rent_tracker/features/repo/Repository.dart';
import 'package:my_rent_tracker/features/repo/SQFliteDatabase.dart';
import 'package:my_rent_tracker/utils/Log.dart';

class PinCodeRepo implements Repository<PinCode> {
  SQFliteDatabase sqFliteDatabase;
  PinCodeDao pinCodeDao;

  PinCodeRepo() {
    Log.v("inside PinCodeRepo");
    init();
  }

  init() async {
    sqFliteDatabase = SQFliteDatabase.getDB();
  }

  void savePinCode(
      PinCode pinCode, StreamSink<SCResponse<String>> sSink) async {
    try {
      pinCodeDao = await sqFliteDatabase.pinCodeDao();

      int pinCodeId = await pinCodeDao
          .persist(PinCode.make(pinCode.pinCodeId, pinCode.pinCode));

      sSink.add(SCResponse.success("Pin Code has been successfully inserted"));
    } catch (ex) {
      sSink.add(SCResponse.error("Couldn't insert pin Code"));
      Log.v(ex.toString());
    }
  }

  @override
  void delete(PinCode entity) {
    // TODO: implement delete
  }

  @override
  void deleteAll() {
    // TODO: implement deleteAll
  }

  @override
  List<PinCode> findAll() {
    // TODO: implement findAll
    return null;
  }

  @override
  PinCode findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  PinCode findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  void persist(PinCode entity) {
    // TODO: implement persist
  }

  @override
  void update(PinCode entity) {
    // TODO: implement update
  }
}
