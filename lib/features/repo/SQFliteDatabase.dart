import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';
import 'package:my_rent_tracker/features/repo/PinCode.dart';
import 'package:my_rent_tracker/features/repo/PinCodeDao.dart';
import 'package:my_rent_tracker/features/repo/Property.dart';
import 'package:my_rent_tracker/features/repo/PropertyDao.dart';
import 'package:my_rent_tracker/features/repo/Tenant.dart';
import 'package:my_rent_tracker/features/repo/TenantDao.dart';
import 'package:my_rent_tracker/features/repo/Transaction.dart';
import 'package:my_rent_tracker/features/repo/TransactionDao.dart';
import 'package:my_rent_tracker/utils/Log.dart';
import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart';

class SQFliteDatabase {
  static SQFliteDatabase _instance;
  static SqfliteAdapter _adapter;

  SQFliteDatabase._();

  static SQFliteDatabase getDB() {
    if (_instance == null) {
      _instance = SQFliteDatabase._();
    }
    return _instance;
  }

  connect() async {
    var dbPath = getDatabasesPath();
    _adapter =
    new SqfliteAdapter(path.join(dbPath.toString(), "myRentTracker.db"));
    await _adapter.connect();

    Log.v("connect");
  }

  close() async {
    await _adapter.close();
  }

  Future<PropertyDao> propertyDao() async {
    Log.v("propertyDao");
    await getDB().connect();
    PropertyBean bean = PropertyBean(_adapter);
    await bean.createTable(ifNotExists: true);
    return PropertyDao.get(bean);
  }

  Future<TenantDao> tenantDao() async {
    Log.v("tenantDao");
    await getDB().connect();
    TenantBean bean = TenantBean(_adapter);
    await bean.createTable(ifNotExists: true);
    return TenantDao.get(bean);
  }

  Future<TransactionDao> transactionDao() async{
    Log.v("transactionDao");
    await getDB().connect();
    TransactionBean transactionBean = TransactionBean(_adapter);
    await transactionBean.createTable(ifNotExists: true);
    return TransactionDao.get(transactionBean);
  }


  Future<PinCodeDao> pinCodeDao() async{
    Log.v("pinCodeDao");
    await getDB().connect();
    PinCodeBean pinCodeBean = PinCodeBean(_adapter);
    await pinCodeBean.createTable(ifNotExists: true);
    return PinCodeDao.get(pinCodeBean);
  }

}
