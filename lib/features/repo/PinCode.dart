
import 'package:jaguar_orm/jaguar_orm.dart';

part 'PinCode.jorm.dart';

class PinCode{
  @PrimaryKey(auto: true)
  int pinCodeId;

  @Column(isNullable: true)
  String pinCode;

  PinCode();

  PinCode.make(this.pinCodeId, this.pinCode);



}

@GenBean()
class PinCodeBean extends Bean<PinCode> with _PinCodeBean {
  PinCodeBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'pincode';
}