import 'package:jaguar_orm/jaguar_orm.dart';

part 'Tenant.jorm.dart';

class Tenant {
  @PrimaryKey(auto: true)
  int tenantId;

  @Column(isNullable: true)
  String mobileNumber;

  @Column(isNullable: true)
  String fullName;

  @Column(isNullable: true)
  String address;

  @Column(isNullable: true)
  String occupancy;

  @Column(isNullable: true)
  String emailAddress;

  @Column(isNullable: true)
  String leaseStartDate;

  @Column(isNullable: true)
  String leaseEndDate;

  @Column(isNullable: true)
  String monthlyValue;

  @Column(isNullable: true)
  String dayValue;

  @Column(isNullable: true)
  String paymentType;

  @Column(isNullable: true)
  String monthlyRent;

  @Column(isNullable: true)
  String electricity;

  @Column(isNullable: true)
  String water;

  @Column(isNullable: true)
  String sanitation;


  Tenant();

  Tenant.make(this.mobileNumber, this.fullName, this.address, this.occupancy,
      this.emailAddress, this.leaseStartDate, this.leaseEndDate,
      this.monthlyValue, this.dayValue, this.paymentType, this.monthlyRent,
      this.electricity, this.water, this.sanitation);


}

@GenBean()
class TenantBean extends Bean<Tenant> with _TenantBean {
  TenantBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'tenant';

}

