// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Property.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _PropertyBean implements Bean<Property> {
  final propertyId = IntField('property_id');
  final name = StrField('name');
  final description = StrField('description');
  final imagePath = StrField('image_path');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        propertyId.name: propertyId,
        name.name: name,
        description.name: description,
        imagePath.name: imagePath,
      };
  Property fromMap(Map map) {
    Property model = Property();
    model.propertyId = adapter.parseValue(map['property_id']);
    model.name = adapter.parseValue(map['name']);
    model.description = adapter.parseValue(map['description']);
    model.imagePath = adapter.parseValue(map['image_path']);

    return model;
  }

  List<SetColumn> toSetColumns(Property model,
      {bool update = false, Set<String> only, bool onlyNonNull: false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      if (model.propertyId != null) {
        ret.add(propertyId.set(model.propertyId));
      }
      ret.add(name.set(model.name));
      ret.add(description.set(model.description));
      ret.add(imagePath.set(model.imagePath));
    } else if (only != null) {
      if (model.propertyId != null) {
        if (only.contains(propertyId.name))
          ret.add(propertyId.set(model.propertyId));
      }
      if (only.contains(name.name)) ret.add(name.set(model.name));
      if (only.contains(description.name))
        ret.add(description.set(model.description));
      if (only.contains(imagePath.name))
        ret.add(imagePath.set(model.imagePath));
    } else /* if (onlyNonNull) */ {
      if (model.propertyId != null) {
        ret.add(propertyId.set(model.propertyId));
      }
      if (model.name != null) {
        ret.add(name.set(model.name));
      }
      if (model.description != null) {
        ret.add(description.set(model.description));
      }
      if (model.imagePath != null) {
        ret.add(imagePath.set(model.imagePath));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists: false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(propertyId.name,
        primary: true, autoIncrement: true, isNullable: false);
    st.addStr(name.name, isNullable: true);
    st.addStr(description.name, isNullable: true);
    st.addStr(imagePath.name, isNullable: true);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(Property model,
      {bool cascade: false, bool onlyNonNull: false, Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(propertyId.name);
    var retId = await adapter.insert(insert);
    if (cascade) {
      Property newModel;
    }
    return retId;
  }

  Future<void> insertMany(List<Property> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(Property model,
      {bool cascade: false, Set<String> only, bool onlyNonNull: false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(propertyId.name);
    var retId = await adapter.upsert(upsert);
    if (cascade) {
      Property newModel;
    }
    return retId;
  }

  Future<void> upsertMany(List<Property> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(Property model,
      {bool cascade: false,
      bool associate: false,
      Set<String> only,
      bool onlyNonNull: false}) async {
    final Update update = updater
        .where(this.propertyId.eq(model.propertyId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<Property> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.propertyId.eq(model.propertyId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<Property> find(int propertyId,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.propertyId.eq(propertyId));
    return await findOne(find);
  }

  Future<int> remove(int propertyId) async {
    final Remove remove = remover.where(this.propertyId.eq(propertyId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<Property> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.propertyId.eq(model.propertyId));
    }
    return adapter.remove(remove);
  }
}
