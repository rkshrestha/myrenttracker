
import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:my_rent_tracker/features/repo/BaseDao.dart';
import 'package:my_rent_tracker/features/repo/Tenant.dart';
import 'package:my_rent_tracker/utils/Log.dart';

class TenantDao implements BaseDao<Tenant>{
  static TenantDao _instance;
  TenantBean bean;

  TenantDao._(this.bean);

  static TenantDao get(TenantBean bean){
    if(_instance==null){
      _instance=TenantDao._(bean);
    }
    return _instance;
  }



  @override
  Future<void> delete(Tenant tenant) {
    Log.v('tenantId:${tenant.tenantId}');
    return bean.remove(tenant.tenantId);
  }

  @override
  Future<void> deleteAll() {
    // TODO: implement deleteAll
    return null;
  }

  @override
  Future<List<Tenant>> findAll() {
    Find findTenant = Find(bean.tableName);
    return bean.findMany(findTenant);
  }

  @override
  Future<Tenant> findById(Object id) {
    return bean.find(id);
  }

  @override
  Future<Tenant> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<int> persist(Tenant tenant) async{
    await bean.createTable(ifNotExists: true);
    Log.v("create Table Tenant");
    return  await bean.insert(tenant);
  }

  @override
  Future<void> update(Tenant entity) {
    // TODO: implement update
    return null;
  }
}