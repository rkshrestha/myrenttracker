import 'dart:async';

import 'package:my_rent_tracker/features/network/SCResponse.dart';
import 'package:my_rent_tracker/features/repo/Repository.dart';
import 'package:my_rent_tracker/features/repo/SQFliteDatabase.dart';
import 'package:my_rent_tracker/features/repo/Tenant.dart';
import 'package:my_rent_tracker/features/repo/TenantDao.dart';
import 'package:my_rent_tracker/utils/Log.dart';

class TenantRepo implements Repository<Tenant> {
  SQFliteDatabase sqFliteDatabase;
  TenantDao tenantDao;
  List<Tenant> tenantList;

  TenantRepo() {
    Log.v("inside TenantRepo");
    init();
  }

  init() async {
    sqFliteDatabase = SQFliteDatabase.getDB();
  }

//

  void getTenantList(StreamSink<SCResponse<List<Tenant>>> stSink) async {
    try {
      tenantDao = await sqFliteDatabase.tenantDao();

      tenantList = await tenantDao.findAll();

      for (Tenant tenant in tenantList) {
        Log.v(tenant.leaseStartDate);
        Log.v(tenant.tenantId.toString() + "," + tenant.fullName);


      }

      stSink.add(SCResponse.success(tenantList));
    } catch (ex) {
      Log.v(ex.toString());
    }
  }

  void saveTenant(Tenant tenant, StreamSink<SCResponse<String>> sSink) async {
    try {
      tenantDao = await sqFliteDatabase.tenantDao();

      int tenantId = await tenantDao.persist(Tenant.make(
          tenant.mobileNumber,
          tenant.fullName,
          tenant.address,
          tenant.occupancy,
          tenant.emailAddress,
          tenant.leaseStartDate,
          tenant.leaseEndDate,
          tenant.monthlyValue,
          tenant.dayValue,
          tenant.paymentType,
          tenant.monthlyRent,
          tenant.electricity,
          tenant.water,
          tenant.sanitation));

      sSink.add(SCResponse.success("Tenant has been successfully inserted"));
    } catch (ex) {
      sSink.add(SCResponse.error("Couldn't insert tenant"));
      Log.v(ex.toString());
    }
  }


  deleteTenant(Tenant tenant,
      StreamSink<SCResponse<String>> sSink) async {
    try {
      tenantDao = await sqFliteDatabase.tenantDao();

      await tenantDao.delete(tenant);

      sSink.add(SCResponse.success("Tenant deleted Successfully"));
    } catch (ex) {
      sSink.add(SCResponse.error("Couldn't delete tenant"));
      Log.v(ex.toString());
    }
  }

    //

  @override
  void delete(Tenant entity) {
    // TODO: implement delete
  }

  @override
  void deleteAll() {
    // TODO: implement deleteAll
  }

  @override
  List<Tenant> findAll() {
    // TODO: implement findAll
    return null;
  }

  @override
  Tenant findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  Tenant findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  void persist(Tenant entity) {
    // TODO: implement persist
  }

  @override
  void update(Tenant entity) {
    // TODO: implement update
  }
}
