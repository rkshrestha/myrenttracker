import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/bloc/propertyBloc/PropertyBloc.dart';
import 'package:my_rent_tracker/features/home/ChangePinCode.dart';
import 'package:my_rent_tracker/features/home/InfoPage.dart';
import 'package:my_rent_tracker/features/home/PinSetPage.dart';
import 'package:my_rent_tracker/features/home/PropertyPage.dart';
import 'package:my_rent_tracker/features/home/PropertyPageList.dart';
import 'package:my_rent_tracker/features/network/SCResponse.dart';
import 'package:my_rent_tracker/features/repo/Property.dart';
import 'package:my_rent_tracker/utils/Constants.dart';
import 'package:my_rent_tracker/utils/DialogUtils.dart';
import 'package:my_rent_tracker/utils/Log.dart';
import 'package:my_rent_tracker/utils/ToastUtils.dart';

class PropertyPageListLogic {
  PropertyPageListState propertyPLState;
  PropertyBloc propertyBloc;
  List<Property> propertyList = [];
  static Property property = Property();

  PropertyPageListLogic(this.propertyPLState, this.propertyBloc);

  void onPostInit() {
    getPropertyList();
  }

  getPropertyList() {
    this.propertyBloc.getPropertyList().listen((it) {
      if (it == null) return;
      Log.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Log.v("property list is " + it.data.toString());
        this.propertyPLState.setState(() {
          this.propertyList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(propertyPLState.context).pop();
        });
      }
    });
  }

//  deleteProperty(property, int position) {
//    this.propertyBloc.deleteProperty(property).listen((it) {
//      if (it == null) return;
//      if (it.status == Status.SUCCESS) {
//        propertyPLState.setState(() {
//          this.propertyList.removeAt(position);
//        });
//
//        ToastUtils.show(it.data);
//      } else if (it.status == Status.ERROR) {
//        ToastUtils.show(it.errorMessage);
//      } else {
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          Navigator.of(propertyPLState.context).pop();
//        });
//      }
//    });
//  }

  confirmationDelete(property, int position) {
    Log.v('getPosition: ' + position.toString());
    Log.v('getIds:' + property.propertyId.toString());

    DialogUtils.showAlertDialog(this.propertyPLState.context, "Alert",
        "Are you sure, you want to delete?", (it) {
      switch (it.eventType) {
        case DialogUtils.ON_CANCELABLE:
          {
            Log.v('No clicked');
            Navigator.pop(this.propertyPLState.context, true);
            break;
          }
        case DialogUtils.ON_CONFIRM:
          {
            Log.v('position property' + position.toString());

            this.propertyBloc.deleteProperty(property).listen((it) {
              if (it == null) return;
              if (it.status == Status.SUCCESS) {
                propertyPLState.setState(() {
                  this.propertyList.removeAt(position);
                });

                Navigator.pop(this.propertyPLState.context, true);
                ToastUtils.show(it.data);
              } else if (it.status == Status.ERROR) {
                ToastUtils.show(it.errorMessage);
              } else {
                new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
                  Navigator.of(propertyPLState.context).pop();
                });
              }
            });

            Log.v('Yes clicked');
            break;
          }
      }
    }, "No", "Yes");
  }

  onEditProperty(Property property, int position) {
    Log.v('get Data:' +
        propertyList[position].propertyId.toString() +
        propertyList[position].name +
        propertyList[position].description);
    Navigator.push(propertyPLState.context,
        MaterialPageRoute(builder: (context) {
      return PropertyPage(property: property, isEdit: true);
    }));
  }

  onAddProperty() {
    Navigator.push(propertyPLState.context,
        MaterialPageRoute(builder: (context) {
      return PropertyPage();
    }));
  }



}
