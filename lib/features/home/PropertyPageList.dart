import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/bloc/blockprovider/BlocProvider.dart';
import 'package:my_rent_tracker/features/bloc/propertyBloc/PropertyBloc.dart';
import 'package:my_rent_tracker/features/home/PropertyPageListLogic.dart';
import 'package:my_rent_tracker/features/home/TenantPageList.dart';
import 'package:my_rent_tracker/features/repo/Property.dart';
import 'package:my_rent_tracker/features/repo/PropertyRepo.dart';
import 'package:my_rent_tracker/utils/Constants.dart';

class PropertyPageList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PropertyPageListState();
  }
}

class PropertyPageListState extends State<PropertyPageList> {
  PropertyPageListLogic propertyListLogic;
  PropertyBloc propertyBloc;
  Property property;
  String choice;

  @override
  void initState() {
    super.initState();
    propertyBloc = PropertyBloc(PropertyRepo());
    propertyListLogic = PropertyPageListLogic(this, propertyBloc);
    propertyListLogic.onPostInit();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.home),
        title: Text('My Rent Tracker'),
        actions: <Widget>[


        ],
      ),
      body: BlocProvider(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 6.0),
              child: Text(
                "Property List",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15.0,
                ),
                textAlign: TextAlign.center,
              ),
            ),

            Expanded(
              child: getPropertyList(),
            ),
            //  getPropertyList(),
          ],
        ),
      ),
      floatingActionButton: Padding(
        padding: EdgeInsets.only(bottom: 35.0),
        child: FloatingActionButton(
            child: Icon(Icons.add),
            tooltip: 'Add Property',
            backgroundColor: Colors.green,
            onPressed: () {
              propertyListLogic.onAddProperty();
            }),
      ),
    );
  }

  ListView getPropertyList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: propertyListLogic.propertyList.length,
        itemBuilder: (BuildContext context, int position) {
          property = this.propertyListLogic.propertyList[position];
          return Dismissible(
            key: Key(this
                .propertyListLogic
                .propertyList[position]
                .propertyId
                .toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
              Property properties = this.propertyListLogic.propertyList[position];
              this.propertyListLogic.confirmationDelete(properties, position);


              //this.propertyListLogic.deleteProperty(property, position);
            },
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ),
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                title: Text(property.name),
                trailing: GestureDetector(
                  child: Icon(
                    Icons.group_add,
                    size: 30.0,
                    color: Colors.black,
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => TenantPageList()));
                  },
                ),
                onTap: () {
                  propertyListLogic.onEditProperty(property, position);
                },
              ),
            ),
          );
        });
  }
}
