

import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/bloc/transactionBloc/TransactionBloc.dart';
import 'package:my_rent_tracker/features/home/TransactionPage.dart';
import 'package:my_rent_tracker/features/home/TransactionPageList.dart';
import 'package:my_rent_tracker/features/network/SCResponse.dart';
import 'package:my_rent_tracker/features/repo/Transaction.dart';
import 'package:my_rent_tracker/utils/DialogUtils.dart';
import 'package:my_rent_tracker/utils/Log.dart';
import 'package:my_rent_tracker/utils/ToastUtils.dart';

class TransactionPageListLogic{

  TransactionListPageState state;
  TransactionBloc transactionBloc;
  static Transaction transaction = Transaction();

  List<Transaction> transactionList = [];

  TransactionPageListLogic(this.state, this.transactionBloc);


  onPostInit(){
    getTransactionList();
  }

   getTransactionList() {
     this.transactionBloc.getTransactionList().listen((it) {
       if (it == null) return;
       Log.v("inside transaction listener it");
       if (it.status == Status.SUCCESS) {
         Log.v("transaction list is " + it.data.toString());
         this.state.setState(() {
           this.transactionList = it.data;
         });
       } else {
         new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
           Navigator.of(state.context).pop();
         });
       }
     });
   }


   //delete


  confirmationDeleteTransaction(transaction, int position) {
    DialogUtils.showAlertDialog(this.state.context, "Alert",
        "Are you sure, you want to delete?", (it) {
          switch (it.eventType) {
            case DialogUtils.ON_CANCELABLE:
              {
                Log.v('No clicked');
                Navigator.pop(this.state.context, true);
                break;
              }
            case DialogUtils.ON_CONFIRM:
              {
                this.transactionBloc.deleteTransaction(transaction).listen((it) {
                  if (it == null) return;
                  if (it.status == Status.SUCCESS) {
                    state.setState(() {
                      this.transactionList.removeAt(position);
                    });

                    Navigator.pop(this.state.context,true);
                    ToastUtils.show(it.data);
                  } else if (it.status == Status.ERROR) {
                    ToastUtils.show(it.errorMessage);
                  } else {
                    new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
                      Navigator.of(state.context).pop();
                    });
                  }
                });

                Log.v('Yes clicked');
                break;
              }
          }
        }, "No", "Yes");
  }




  onEditTransaction(Transaction transaction, int position) {
    Navigator.push(state.context,
        MaterialPageRoute(builder: (context) {
          return TransactionPage(transaction: transaction, isEdit: true);
        }));
  }

  onAddTransaction() {
    Navigator.push(state.context,
        MaterialPageRoute(builder: (context) {
          return TransactionPage();
        }));
  }
}