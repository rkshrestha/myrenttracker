import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/bloc/blockprovider/BlocProvider.dart';

class ChangePinCode extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ChangePinCodeState();
  }
}

class ChangePinCodeState extends State<ChangePinCode> {
  final changePinKey = GlobalKey<FormState>();

  Widget changePinForm() {
    //tf old pin
    TextFormField tfOldPin = TextFormField(
        decoration: InputDecoration(
          labelText: 'Old Pin Code*',
        ),
        onSaved: (String oldPin) {},
        validator: (oldPin) {
          if (oldPin.isEmpty) {
            return "Old Pin Code can't be empty";
          }
        });

    //tf New Pin
    TextFormField tfNewPin = TextFormField(
        decoration: InputDecoration(
          labelText: 'New Pin Code*',
        ),
        onSaved: (String newPin) {},
        validator: (newPin) {
          if (newPin.isEmpty) {
            return "New Pin Code can't be empty";
          }
        });

    //tf new pin confirm
    TextFormField tfPinConfirm = TextFormField(
        decoration: InputDecoration(
          labelText: 'Confirm Pin Code*',
        ),
        onSaved: (String confirmPin) {},
        validator: (confirmPin) {
          if (confirmPin.isEmpty) {
            return "Confirm Pin Code can't be empty";
          }
        });

    // rb save
    RaisedButton rbSave = RaisedButton(
      child: Text('Save'),
      color: Colors.green,
      onPressed: () {
        if(changePinKey.currentState.validate()){
          changePinKey.currentState.save();
        }

      },
    );

    return Form(
      key: changePinKey,
      child: Padding(
        padding: EdgeInsets.only(top: 10.0, left: 14.0, right: 14.0),
        child: Column(
          children: <Widget>[
            tfOldPin,
            Padding(
              padding: EdgeInsets.only(top: 4.0),
              child: tfNewPin,
            ),
            Padding(
              padding: EdgeInsets.only(top: 4.0),
              child: tfPinConfirm,
            ),
            Padding(
              padding: EdgeInsets.only(top: 8.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: rbSave,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Change Pin Code'),
      ),
      body: BlocProvider(
        child: ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 6.0),
              child: Text(
                'Pin Code must contain four digits',
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            changePinForm(),
          ],
        ),
      ),
    );
  }
}
