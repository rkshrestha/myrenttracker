import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/bloc/blockprovider/BlocProvider.dart';
import 'package:my_rent_tracker/features/bloc/transactionBloc/TransactionBloc.dart';
import 'package:my_rent_tracker/features/home/PinSetPage.dart';
import 'package:my_rent_tracker/features/home/TransactionPageListLogic.dart';
import 'package:my_rent_tracker/features/repo/Transaction.dart';
import 'package:my_rent_tracker/features/repo/TransactionRepo.dart';

class TransactionListPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TransactionListPageState();
  }
}

class TransactionListPageState extends State<TransactionListPage> {
  TransactionPageListLogic logic;
  TransactionBloc transactionBloc;
  Transaction transaction;

  @override
  void initState() {
    super.initState();
    transactionBloc = TransactionBloc(TransactionRepo());
    logic = TransactionPageListLogic(this, transactionBloc);
    this.logic.onPostInit();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bill and Payment of #tenantName'),
      ),
      body: BlocProvider(
        child: Column(
          children: <Widget>[
            Table(
              children: [
                TableRow(children: [
                  TableCell(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(""),
                        Text("Date"),
                        Text("Amount"),
                        Text("Paid"),
                        Text("Due"),
                        Text(""),
                      ],
                    ),
                  ),

                  //data
                ]),
              ],
            ),
            Expanded(
              child: getTransactionList(),
            ),
          ],
        ),
      ),
      floatingActionButton: Padding(
        padding: EdgeInsets.only(bottom: 35.0),
        child: FloatingActionButton(
            child: Icon(Icons.add),
            tooltip: 'Add Property',
            backgroundColor: Colors.green,
            onPressed: () {
              logic.onAddTransaction();
//              Navigator.push(context,
//                  MaterialPageRoute(builder: (context) => TransactionPage()));
            }),
      ),
    );
  }

  ListView getTransactionList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: this.logic.transactionList.length,
        itemBuilder: (BuildContext context, int position) {
          Transaction transaction = this.logic.transactionList[position];
          return Dismissible(
            key: Key(
                this.logic.transactionList[position].transactionId.toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
              Transaction transactions = this.logic.transactionList[position];

              this.logic.confirmationDeleteTransaction(transactions, position);
            },
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ),
            child: Card(
              child: Table(
                children: [
                  TableRow(children: [
                    TableCell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          IconButton(
                              icon: (Icon(
                                Icons.edit,
                                color: Colors.green,
                              )),
                              onPressed: () {
                                logic.onEditTransaction(transaction, position);
                              }),
                          Text(transaction.date),
                          Text(transaction.monthlyRent),
                          Text(transaction.paid == null
                              ? "0"
                              : transaction.paid),
                          Text(transaction.dueAmount),
                          IconButton(
                              icon: (Icon(Icons.add, color: Colors.green)),
                              onPressed: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>PinSetPage()));

                              }),
                        ],
                      ),
                    ),

                    //data
                  ]),
                ],
              ),
            ),
          );
        });
  }
}
