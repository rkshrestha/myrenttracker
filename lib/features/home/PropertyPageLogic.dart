import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_rent_tracker/features/bloc/propertyBloc/PropertyBloc.dart';
import 'package:my_rent_tracker/features/home/PropertyPage.dart';
import 'package:my_rent_tracker/features/network/SCResponse.dart';
import 'package:my_rent_tracker/features/repo/Property.dart';
import 'package:my_rent_tracker/utils/FileUtils.dart';
import 'package:my_rent_tracker/utils/Log.dart';
import 'package:my_rent_tracker/utils/ToastUtils.dart';

class PropertyPageLogic {
  PropertyPageState pageState;
  PropertyBloc propertyBloc;
  Property property = Property();
  File imgFileSelected;

  PropertyPageLogic(this.pageState, this.propertyBloc);

  void onPostState() {
    if (this.pageState.isEdit) {
      this.pageState.setAppBar('Edit Property');

      Log.v('inside ' + this.pageState.isEdit.toString());
    } else {
      this.pageState.setAppBar('Add Property');
      Log.v('inside ' + this.pageState.isEdit.toString());
    }
  }

  //image picker from camera
  pickerCamera() async {
    imgFileSelected = await ImagePicker.pickImage(
        source: ImageSource.camera, maxWidth: 1000.0, maxHeight: 250.0);

    if (imgFileSelected != null) {
      this.pageState.setState(() {});
    }
  }

  //image picker from gallery
  pickerGallery() async {
    imgFileSelected = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxWidth: 1000.0, maxHeight: 250.0);

    if (imgFileSelected != null) {
      this.pageState.setState(() {});
    }
  }

  saveProperty() async {
    await FileUtils.getDir(FileUtils.PROPERTY_DIR);

//   File savedFile= await imgFileSelected.copy('/storage/emulated/0/Android/data/com.pathway.my_rent_tracker/files/Pictures/scaled_00a362fb-8e28-45b8-b8a7-cdcd368bb9a77939647074404028452.jpg');
//    this.property.imagePath=savedFile.path;

    FileUtils.saveFile(imgFileSelected, "a.jpg");

    this.propertyBloc.saveProperty(this.property).listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        ToastUtils.show(it.data);
        Navigator.pop(this.pageState.context);
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(pageState.context).pop();
        });
      }
    });
  }

  updateProperty() async {
    await FileUtils.getDir(FileUtils.PROPERTY_DIR);
    FileUtils.saveFile(imgFileSelected, "b.jpg");

    this.propertyBloc.updateProperty(this.property).listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        ToastUtils.show(it.data);
        Navigator.pop(this.pageState.context);
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(pageState.context).pop();
        });
      }
    });
  }
}
