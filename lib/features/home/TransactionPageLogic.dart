
import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/bloc/transactionBloc/TransactionBloc.dart';
import 'package:my_rent_tracker/features/home/TransactionPage.dart';
import 'package:my_rent_tracker/features/network/SCResponse.dart';
import 'package:my_rent_tracker/features/repo/Transaction.dart';
import 'package:my_rent_tracker/utils/Log.dart';
import 'package:my_rent_tracker/utils/ToastUtils.dart';

class TransactionPageLogic {

  TransactionPageState transactionPageState;
  TransactionBloc transactionBloc;
  Transaction transaction = Transaction();

  TransactionPageLogic(this.transactionPageState, this.transactionBloc);


  void onPostState() {
    if (this.transactionPageState.isEdit) {
      this.transactionPageState.setAppBar('Edit Transaction');

      Log.v('inside ' + this.transactionPageState.isEdit.toString());
    } else {
      this.transactionPageState.setAppBar('Add Transaction');
      Log.v('inside ' + this.transactionPageState.isEdit.toString());
    }
  }

  saveTransaction() async {
    this.transactionBloc.saveTransaction(this.transaction).listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        ToastUtils.show(it.data);
        Navigator.pop(this.transactionPageState.context);
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(transactionPageState.context).pop();
        });
      }
    });
  }


}
