import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/bloc/blockprovider/BlocProvider.dart';
import 'package:my_rent_tracker/features/bloc/transactionBloc/TransactionBloc.dart';
import 'package:my_rent_tracker/features/home/TransactionPageLogic.dart';
import 'package:my_rent_tracker/features/repo/Transaction.dart';
import 'package:my_rent_tracker/features/repo/TransactionRepo.dart';
import 'package:my_rent_tracker/utils/Log.dart';

class TransactionPage extends StatefulWidget {
  bool isEdit;
  Transaction transaction;


  TransactionPage( {this.transaction,this.isEdit = false});

  @override
  State<StatefulWidget> createState() {
    return TransactionPageState(transaction: this.transaction, isEdit: this.isEdit);
  }
}

class TransactionPageState extends State<TransactionPage> {
  bool isEdit;
  Transaction transaction;
  final rentalFormKey = GlobalKey<FormState>();
  TransactionPageLogic logic;
  TransactionBloc transactionBloc;
  AppBar appBar;



  TransactionPageState({this.transaction,this.isEdit= false});

  @override
  void initState() {
    super.initState();

    this.transactionBloc = TransactionBloc(TransactionRepo());
    this.logic = TransactionPageLogic(this, transactionBloc);
    this.logic.onPostState();

  }


  setAppBar(String title) {
    appBar = AppBar(title: Text(title));
  }

  Widget rentalForm() {
    Transaction transaction = this.logic.transaction;

    //tf date
    TextFormField tfDate = TextFormField(
      keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: 'Date',
        ),
        onSaved: (String date) {
        transaction.date = date;
        },
        validator: (date) {
          if (date.isEmpty) {
            return "Date can't be empty";
          }
        });

    //tf rent
    TextFormField tfRent = TextFormField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: 'Rent',
        ),
        onSaved: (String rent) {
          transaction.monthlyRent = rent;
        },
        validator: (rent) {
          if (rent.isEmpty) {
            return "Rent can't be empty";
          }
        });

    //tfElectricity
    TextFormField tfElectricity = TextFormField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: 'Electricity',
        ),
        onSaved: (String electricity) {
          transaction.electricity = electricity;
        },
        validator: (electricity) {
          if (electricity.isEmpty) {
            return "Electricity can't be empty";
          }
        });

    // tf water
    TextFormField tfWater = TextFormField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: 'Water',
        ),
        onSaved: (String water) {
          transaction.water = water;
        },
        validator: (water) {
          if (water.isEmpty) {
            return "Water can't be empty";
          }
        });

    //tf sanitation
    TextFormField tfSanitation = TextFormField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: 'Sanitation',
        ),
        onSaved: (String sanitation) {
          transaction.sanitation = sanitation;
        },
        validator: (sanitation) {
          if (sanitation.isEmpty) {
            return "Sanitation can't be empty";
          }
        });

    //tf due amount
    TextFormField tfDueAmount = TextFormField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: 'Due Amount',
          hintText: 'Enter 0, if no any due amount',
        ),
        onSaved: (String dueAmount) {
          transaction.dueAmount = dueAmount;
        },
        validator: (dueAmount) {
          if (dueAmount.isEmpty) {
            return "Due Amount can't be empty";
          }
        });

    bool isChecked = false;

    //checkbox sms
    Checkbox cbSms = Checkbox(
      value: isChecked,
      onChanged: (clicked) {},
    );

    //rb save
    RaisedButton rbSave = RaisedButton(
      child: Text('Save'),
      color: Colors.green,
      onPressed: () {
        if (rentalFormKey.currentState.validate()) {
          rentalFormKey.currentState.save();
          this.logic.saveTransaction();
          Log.v("Transaction saved......");
        }

      },
    );


    //rb update
    RaisedButton rbUpdate = RaisedButton(
      child: Text('Update'),
      color: Colors.green,
      onPressed: () {
        if (rentalFormKey.currentState.validate()) {
          rentalFormKey.currentState.save();

        }

      },
    );

    //astrick text
    Text astrickText = Text(
      '*',
      style: TextStyle(
        color: Colors.red,
        fontWeight: FontWeight.bold,
        fontSize: 18.0,
      ),
    );
    //



    return Form(
      key: rentalFormKey,
      child: Padding(
        padding: EdgeInsets.only(top: 10.0, left: 8.0, right: 8.0),
        child: Column(

          children: <Widget>[
            ListTile(
              leading: const Icon(Icons.date_range),
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: tfDate,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              leading: const Icon(Icons.receipt),
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: tfRent,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              leading: const Icon(Icons.linear_scale),
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: tfElectricity,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              leading: const Icon(Icons.branding_watermark),
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: tfWater,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              leading: const Icon(Icons.border_style),
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: tfSanitation,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              leading: const Icon(Icons.crop_original),
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: tfDueAmount,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 14.0),
              child: Row(
                children: <Widget>[
                  Text(
                    'Total Amount: ',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text('#amount'),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 2.0),
              child: Row(
                children: <Widget>[
                  cbSms,
                  Text('Send Bill as SMS'),
                ],
              ),
            ),
//            Padding(
//              padding: EdgeInsets.only(top: 1.0),
//              child: rbSave,
//            ),

            Padding(
              padding: EdgeInsets.all(10.0),
              //  padding: EdgeInsets.only(top: 12.0),
              child: Row(
                children: <Widget>[
//                  Visibility(
//                    child: rbCancel,
//                  ),
                  Container(
                    margin: EdgeInsets.only(right: 5.0),
                  ),
                  Visibility(
                    visible: !this.isEdit,
                    child: rbSave,
                  ),
                  Visibility(
                    visible: this.isEdit,
                    child: rbUpdate,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar,
      body: BlocProvider(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 8.0),
              child: Text(
                'Rental Bill For #name',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),

            Expanded(
              child: NotificationListener<ScrollNotification>(
                onNotification: (scrollNotification) {
                //  Log.v("scrolled");
                },
                child: SingleChildScrollView(
                  child: rentalForm(),
                ),
              ),

              //
            ),

          ],
        ),
      ),
    );
  }
}
