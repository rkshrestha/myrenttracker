import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/bloc/PinCodeBloc.dart';
import 'package:my_rent_tracker/features/home/PinSetPage.dart';
import 'package:my_rent_tracker/features/network/SCResponse.dart';
import 'package:my_rent_tracker/features/repo/PinCode.dart';
import 'package:my_rent_tracker/utils/ToastUtils.dart';

class PinSetPageLogic {
  PinSetPageState pageState;
  PinCodeBloc pinCodeBloc;
  PinCode pinCode = PinCode();

  PinSetPageLogic(this.pageState, this.pinCodeBloc);

  savePinCode() async {
    this.pinCodeBloc.savePinCode(this.pinCode).listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        ToastUtils.show(it.data);
        Navigator.pop(this.pageState.context);
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(pageState.context).pop();
        });
      }
    });
  }
}
