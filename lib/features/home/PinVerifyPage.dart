import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/bloc/blockprovider/BlocProvider.dart';

class PinVerifyPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PinVerifyPageState();
  }
}

class PinVerifyPageState extends State<PinVerifyPage> {
  final pinVerifyKey = GlobalKey<FormState>();

  Widget pinVerifyForm() {
    //Enter pin code tf
    TextFormField tfPinCode = TextFormField(
        onSaved: (String pinVerify) {},
        validator: (pinVerify) {
          if (pinVerify.isEmpty) {
            return "Pin Code can't be empty";
          }
        });

    // Cancel button
    RaisedButton rbCancel = RaisedButton(
      child: Text('Cancel'),
      color: Colors.green,
      onPressed: () {},
    );

    //ok button
    RaisedButton rbOk = RaisedButton(
      child: Text('Ok'),
      color: Colors.green,
      onPressed: () {},
    );

//  button finger print
    RaisedButton rbFingerPrint = RaisedButton(
      child: Icon(Icons.fingerprint),
      color: Colors.green,
      onPressed: () {},
    );

    return Form(
      key: pinVerifyKey,
      child: Column(
        children: <Widget>[
          tfPinCode,
          Padding(
            padding: EdgeInsets.only(top: 16.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: rbCancel,
                ),
                Container(
                  margin: EdgeInsets.only(right: 5.0),
                ),
                Expanded(
                  child: rbOk,
                ),
                Container(
                  margin: EdgeInsets.only(right: 5.0),
                ),
                Expanded(
                  child: rbFingerPrint,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.lock),
        title: Text("Confirm Pin"),
      ),
      body: BlocProvider(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 30.0),
              child: Text(
                'Enter Pin Code',
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10.0, left: 24.0, right: 24.0),
              child: pinVerifyForm(),
            ),
          ],
        ),
      ),
    );
  }
}
