import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/bloc/tenantBloc/TenantBloc.dart';
import 'package:my_rent_tracker/features/home/TenantPage.dart';
import 'package:my_rent_tracker/features/home/TenantPageList.dart';
import 'package:my_rent_tracker/features/network/SCResponse.dart';
import 'package:my_rent_tracker/features/repo/Tenant.dart';
import 'package:my_rent_tracker/utils/DialogUtils.dart';
import 'package:my_rent_tracker/utils/Log.dart';
import 'package:my_rent_tracker/utils/ToastUtils.dart';

class TenantPageListLogic {
  TenantPageListState tenantPageListState;
  TenantBloc tenantBloc;
  static Tenant tenant = Tenant();
  List<Tenant> tenantList = [];

  TenantPageListLogic(this.tenantPageListState, this.tenantBloc);

  void onPostInit() {
    getTenantList();
  }

  getTenantList() {
    this.tenantBloc.getTenantList().listen((it) {
      if (it == null) return;
      Log.v("inside tenant listener it");
      if (it.status == Status.SUCCESS) {
        Log.v("tenant list is " + it.data.toString());
        this.tenantPageListState.setState(() {
          this.tenantList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(tenantPageListState.context).pop();
        });
      }
    });
  }

//  deleteTenant(tenant, int position) {
//    this.tenantBloc.deleteTenant(tenant).listen((it) {
//      if (it == null) return;
//      if (it.status == Status.SUCCESS) {
//        tenantPageListState.setState(() {
//          this.tenantList.removeAt(position);
//        });
//
//        ToastUtils.show(it.data);
//      } else if (it.status == Status.ERROR) {
//        ToastUtils.show(it.errorMessage);
//      } else {
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          Navigator.of(tenantPageListState.context).pop();
//        });
//      }
//    });
//  }

  confirmationDeleteTenant(tenant, int position) {
    DialogUtils.showAlertDialog(this.tenantPageListState.context, "Alert",
        "Are you sure, you want to delete?", (it) {
      switch (it.eventType) {
        case DialogUtils.ON_CANCELABLE:
          {
            Log.v('No clicked');
            Navigator.pop(this.tenantPageListState.context, true);
            break;
          }
        case DialogUtils.ON_CONFIRM:
          {
            this.tenantBloc.deleteTenant(tenant).listen((it) {
              if (it == null) return;
              if (it.status == Status.SUCCESS) {
                tenantPageListState.setState(() {
                  this.tenantList.removeAt(position);
                });

                Navigator.pop(this.tenantPageListState.context, true);
                ToastUtils.show(it.data);
              } else if (it.status == Status.ERROR) {
                ToastUtils.show(it.errorMessage);
              } else {
                new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
                  Navigator.of(tenantPageListState.context).pop();
                });
              }
            });

            Log.v('Yes clicked');
            break;
          }
      }
    }, "No", "Yes");
  }

  onEditTenant(Tenant tenant, int position) {
    Navigator.push(tenantPageListState.context,
        MaterialPageRoute(builder: (context) {
      return TenantPage(tenant: tenant, isEdit: true);
    }));
  }

  onAddTenant() {
    Navigator.push(tenantPageListState.context,
        MaterialPageRoute(builder: (context) {
      return TenantPage();
    }));
  }
}
