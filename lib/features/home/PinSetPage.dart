import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/bloc/PinCodeBloc.dart';
import 'package:my_rent_tracker/features/bloc/blockprovider/BlocProvider.dart';
import 'package:my_rent_tracker/features/home/PinSetPageLogic.dart';
import 'package:my_rent_tracker/features/repo/PinCodeRepo.dart';

class PinSetPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PinSetPageState();
  }
}

class PinSetPageState extends State<PinSetPage> {
  final pinCodeFormKey = GlobalKey<FormState>();

  PinSetPageLogic pinLogic;
  PinCodeBloc pinCodeBloc;

  @override
  void initState() {
    super.initState();

    this.pinCodeBloc = PinCodeBloc(PinCodeRepo());
    this.pinLogic = PinSetPageLogic(this, pinCodeBloc);
  }

  Widget pinSetForm() {
    //pin code tf
    TextFormField tfPin = TextFormField(
        decoration: InputDecoration(
          labelText: 'Pin Code*',
        ),
        onSaved: (String pinCode) {},
        validator: (pinCode) {
          if (pinCode.isEmpty) {
            return "Pin Code can't be empty";
          } else if (pinCode.length != 4) {
            return "Pin Code must be four digits.";
          }
        });

    //pin code confirm tf
    TextFormField tfPinConfirm = TextFormField(
        decoration: InputDecoration(
          labelText: 'Confirm Pin Code*',
        ),
        onSaved: (String pinConfirm) {},
        validator: (pinConfirm) {
          if (pinConfirm.isEmpty) {
            return "Confirmation of Pin Code can't be empty";
          }
        });

    // Cancel Button
    RaisedButton rbCancel = RaisedButton(
        child: Text('Cancel'),
        color: Colors.green,
        onPressed: () {
          Navigator.pop(context);
        });

    //Save Button
    RaisedButton rbSave = RaisedButton(
      child: Text('Save'),
      color: Colors.green,
      onPressed: () {
        if (pinCodeFormKey.currentState.validate()) {
          pinCodeFormKey.currentState.save();
          this.pinLogic.savePinCode();
        }
      },
    );

    return Form(
      key: pinCodeFormKey,
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: tfPin,
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: tfPinConfirm,
          ),
          Padding(
            padding: EdgeInsets.only(top: 12.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: rbCancel,
                ),
                Container(
                  margin: EdgeInsets.only(right: 5.0),
                ),
                Expanded(child: rbSave),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(
          Icons.lock,
          size: 28.0,
        ),
        title: Text("SET PIN"),
      ),
      body: BlocProvider(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 20.0),
              child: Text(
                'Remember this Pin Code. If you forget it,you need \n\n'
                    ' to reinstall the application and all data will be erased.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 14.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20.0),
            ),
            Padding(
              padding: EdgeInsets.all(1.0),
              child: Text(
                'Pin code must contain four digit',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 20.0, right: 20.0),
              child: pinSetForm(),
            ),
          ],
        ),
      ),
    );
  }
}
