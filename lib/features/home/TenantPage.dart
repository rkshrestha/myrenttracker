import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/bloc/blockprovider/BlocProvider.dart';
import 'package:my_rent_tracker/features/bloc/tenantBloc/TenantBloc.dart';
import 'package:my_rent_tracker/features/home/TenantPageLogic.dart';
import 'package:my_rent_tracker/features/repo/Tenant.dart';
import 'package:my_rent_tracker/features/repo/TenantRepo.dart';

class TenantPage extends StatefulWidget {
  bool isEdit;
  Tenant tenant;

  TenantPage({this.tenant, this.isEdit = false});

  @override
  State<StatefulWidget> createState() {
    return TenantPageState(tenant: this.tenant, isEdit: this.isEdit);
  }
}

class TenantPageState extends State<TenantPage> {
  bool isEdit;
  static var paymentType = ['Monthly'];

  GlobalKey<FormState> tenantFormKey = GlobalKey<FormState>();
  TenantPageLogic logic;
  TenantBloc tenantBloc;
  Tenant tenant;
  AppBar appBar;
  TextEditingController mblController = TextEditingController();

  TenantPageState({this.tenant, this.isEdit = false});

  @override
  void initState() {
    super.initState();
    this.tenantBloc = TenantBloc(TenantRepo());
    this.logic = TenantPageLogic(this, tenantBloc);
    this.logic.onPostState();
  }

  setAppBar(String title) {
    appBar = AppBar(title: Text(title));
  }

  Widget getTenantForm() {
    Tenant tenant = this.logic.tenant;

    //mobile Number textFormField
    TextFormField tfMblNumber = TextFormField(
        keyboardType: TextInputType.phone,
        //controller: mblController,
        decoration: InputDecoration(
          labelText: 'Mobile Number',
        ),
        onSaved: (mblNumber) {
          tenant.mobileNumber = mblNumber;
        },
        validator: (mblNumber) {
          if (mblNumber.isEmpty) {
            return "Mobile Number can't be empty";
          }
        });

    //full Name textFormField
    TextFormField tffullName = TextFormField(
        decoration: InputDecoration(
          labelText: 'Full Name',
        ),
        onSaved: (String fullName) {
          tenant.fullName = fullName;
        },
        validator: (fullName) {
          if (fullName.isEmpty) {
            return "Full Name can't be empty";
          }
        });

    //address TextFormField
    TextFormField tfAddress = TextFormField(
        decoration: InputDecoration(
          labelText: 'Address',
        ),
        onSaved: (String address) {
          tenant.address = address;
        },
        validator: (address) {
          if (address.isEmpty) {
            return "Address can't be empty";
          }
        });

    //Occupancy detail textFOrmField
    TextFormField tfOccupancy = TextFormField(
        decoration: InputDecoration(
          labelText: 'Occupancy',
          hintText: 'Floor details',
        ),
        onSaved: (String occupancy) {
          tenant.occupancy = occupancy;
        },
        validator: (occupancy) {
          if (occupancy.isEmpty) {
            return "Occupancy can't be empty";
          }
        });

    //email address textFormField
    TextFormField tfEmail = TextFormField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          labelText: 'Email Address',
        ),
        onSaved: (String email) {
          tenant.emailAddress = email;
        },
        validator: (email) {
//          if (email.isEmpty) {
//            return "Email Address can't be empty";
//          }
        });

    //date picker

    // DateTimePickerFormField

    //Lease Start date textFormField
    TextFormField tfLeaseStart = TextFormField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: 'Lease Start Date',
        ),
        onSaved: (String leaseStart) {
          tenant.leaseStartDate = leaseStart;
        },
        validator: (leaseStart) {
          if (leaseStart.isEmpty) {
            return "Lease Start Date can't be empty";
          }
        });

    //Lease End date textFormField
    TextFormField tfLeaseEnd = TextFormField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: 'Lease End Date',
        ),
        onSaved: (String leaseEnd) {
          tenant.leaseEndDate = leaseEnd;
        },
        validator: (leaseEnd) {
          if (leaseEnd.isEmpty) {
            return "Lease End Date can't be empty";
          }
        });

    //Rent Paid Date
    //Rent paid Monthly textFormField
    TextFormField tfEveryMonth = TextFormField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          hintText: 'Month',
        ),
        onSaved: (String month) {
          tenant.monthlyValue = month;
        },
        validator: (month) {
          if (month.isEmpty) {
            return "Month can't be empty";
          }
        });

    //Rent paid Day textFormField
    TextFormField tfEveryDay = TextFormField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          hintText: 'Day',
        ),
        onSaved: (String day) {
          tenant.dayValue = day;
        },
        validator: (day) {
          if (day.isEmpty) {
            return "Day can't be empty";
          }
        });

    //Payment System

    //type: dropbox
    DropdownButton dbPaymentType = DropdownButton(
        items: paymentType.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Text(dropDownStringItem),
          );
        }).toList(),
        //  value: dropDownStringItem,
        value: 'Monthly',
        onChanged: (valueSelectedByUser) {
          setState(() {
            //   updatePriorityAsInt(valueSelectedByUser);
            //  debugPrint("show $valueSelectedByUser");
          });
        });

    //Monthly Rent textFormField
    TextFormField tfMonthlyRent = TextFormField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: 'Monthly Rent',
        ),
        onSaved: (monthlyRent) {
          tenant.monthlyRent = monthlyRent;
        },
        validator: (monthlyRent) {
          if (monthlyRent.isEmpty) {
            return "Monthly Rent can't be empty";
          }
        });

    //Electricity textFormField
    TextFormField tfElectricity = TextFormField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: 'Electricity',
        ),
        onSaved: (String electricity) {
          tenant.electricity = electricity;
        },
        validator: (electricity) {
          if (electricity.isEmpty) {
            return "Electricity can't be empty";
          }
        });

    //Water textFormField
    TextFormField tfWater = TextFormField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: 'Water',
        ),
        onSaved: (String water) {
          tenant.water = water;
        },
        validator: (water) {
          if (water.isEmpty) {
            return "Water can't be empty";
          }
        });

    //sanitation textFormField
    TextFormField tfSanitation = TextFormField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: 'Sanitation',
        ),
        onSaved: (String sanitation) {
          tenant.sanitation = sanitation;
        },
        validator: (sanitation) {
          if (sanitation.isEmpty) {
            return "Sanitation can't be empty";
          }
        });

    //Raised Button Cancel
    RaisedButton rbCancel = RaisedButton(
        child: Text('Cancel'),
        color: Colors.green,
        onPressed: () {
          Navigator.pop(context);
        });

    //Raised Save Button
    RaisedButton rbSave = RaisedButton(
      child: Text('Save'),
      color: Colors.green,
      onPressed: () {
        if (tenantFormKey.currentState.validate()) {
          tenantFormKey.currentState.save();
          this.logic.saveTenant();
        }
      },
    );

    //Raised Update Button
    RaisedButton rbUpdate = RaisedButton(
      child: Text('Update'),
      color: Colors.green,
      onPressed: () {
        if (tenantFormKey.currentState.validate()) {
          tenantFormKey.currentState.save();
        }
      },
    );

    Text astrickText = Text(
      '*',
      style: TextStyle(
        color: Colors.red,
        fontWeight: FontWeight.bold,
        fontSize: 18.0,
      ),
    );
    //

    return Form(
      key: tenantFormKey,
      child: Column(
        children: <Widget>[
//          ListTile(
//            title: Text(
//              logic.contact == null
//                  ? 'No contact selected.'
//                  : logic.contact.phoneNumber.number.toString(),
//            ),
//          ),
          ListTile(
            leading: const Icon(Icons.contact_phone),
            title: Row(
              children: <Widget>[
                Expanded(
                  flex: 7,
                  child: tfMblNumber,
                ),
                Expanded(
                  flex: 1,
                  child: astrickText,
                ),
              ],
            ),
            trailing: IconButton(
                icon: Icon(Icons.contacts),
                onPressed: () {
                  logic.getContact();
                }),
          ),
          ListTile(
            leading: const Icon(Icons.person),
            title: Row(
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: tffullName,
                ),
                Expanded(
                  flex: 1,
                  child: astrickText,
                ),
              ],
            ),
          ),
          ListTile(
            leading: const Icon(Icons.location_city),
            title: Row(
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: tfAddress,
                ),
                Expanded(
                  flex: 1,
                  child: astrickText,
                ),
              ],
            ),
          ),
          ListTile(
            leading: Icon(Icons.store),
            title: Row(
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: tfOccupancy,
                ),
                Expanded(
                  flex: 1,
                  child: astrickText,
                ),
              ],
            ),
          ),
          ListTile(
            leading: Icon(Icons.email),
            title: Row(
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: tfEmail,
                ),
                Expanded(
                  flex: 1,
                  child: Text(''),
                ),
              ],
            ),
          ),
          ListTile(
              leading: Icon(Icons.date_range),
              title: InkWell(
                onTap: () {},
                //   child: IgnorePointer(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 6,
                      child: tfLeaseStart,
                    ),
                    Expanded(
                      flex: 1,
                      child: astrickText,
                    ),
                  ],
                ),
                //      ),
              )

//            trailing: IconButton(
//                icon: Icon(Icons.date_range),
//                onPressed: () {
//                  logic.getDate();
//                }),
              ),
          ListTile(
            leading: Icon(Icons.date_range),
            title: InkWell(
              onTap: () {},
              //     child: IgnorePointer(
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 6,
                    child: tfLeaseEnd,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
              //   ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 12.0),
            child: Text(
              'How often is Rent Paid?*',
              style: TextStyle(fontWeight: FontWeight.bold),
              textAlign: TextAlign.start,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Text('Every'),
                ),
                Expanded(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 6,
                        child: tfEveryMonth,
                      ),
                      Expanded(
                        flex: 1,
                        child: astrickText,
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Text('Month(s) On'),
                ),
                Expanded(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 6,
                        child: tfEveryDay,
                      ),
                      Expanded(
                        flex: 1,
                        child: astrickText,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 12.0),
            child: Text(
              'Payment System',
              style: TextStyle(fontWeight: FontWeight.bold),
              textAlign: TextAlign.start,
            ),
          ),
          ListTile(leading: const Icon(Icons.payment), title: dbPaymentType),
          ListTile(
            leading: Icon(Icons.receipt),
            title: Row(
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: tfMonthlyRent,
                ),
                Expanded(
                  flex: 1,
                  child: astrickText,
                ),
              ],
            ),
          ),
          ListTile(
            leading: const Icon(Icons.linear_scale),
            title: Row(
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: tfElectricity,
                ),
                Expanded(
                  flex: 1,
                  child: astrickText,
                ),
              ],
            ),
          ),
          ListTile(
            leading: const Icon(Icons.branding_watermark),
            title: Row(
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: tfWater,
                ),
                Expanded(
                  flex: 1,
                  child: astrickText,
                ),
              ],
            ),
          ),
          ListTile(
            leading: const Icon(Icons.border_style),
            title: Row(
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: tfSanitation,
                ),
                Expanded(
                  flex: 1,
                  child: astrickText,
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10.0),
            //  padding: EdgeInsets.only(top: 12.0),
            child: Row(
              children: <Widget>[
//                Visibility(
//                  child: rbCancel,
//                ),
                Container(
                  margin: EdgeInsets.only(right: 5.0),
                ),
                Visibility(
                  visible: !this.isEdit,
                  child: rbSave,
                ),
                Visibility(
                  visible: this.isEdit,
                  child: rbUpdate,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  //listview
//
//  Widget tenantForm() {
//    Tenant tenant;
//    setState(() {
//      tenant = this.logic.tenant;
//    });
//
//    //mobile Number textFormField
//    TextFormField tfMblNumber = TextFormField(
//        keyboardType: TextInputType.phone,
//        decoration: InputDecoration(
//          labelText: 'Mobile Number*',
//        ),
//        onSaved: (String mblNumber) {
//          tenant.mobileNumber = mblNumber;
//        },
//        validator: (mblNumber) {
//          if (mblNumber.isEmpty) {
//            return "Mobile Number can't be empty";
//          }
//        });
//
//    //full Name textFormField
//    TextFormField tffullName = TextFormField(
//        decoration: InputDecoration(
//          labelText: 'Full Name*',
//        ),
//        onSaved: (String fullName) {
//          tenant.fullName = fullName;
//        },
//        validator: (fullName) {
//          if (fullName.isEmpty) {
//            return "Full Name can't be empty";
//          }
//        });
//
//    //address TextFormField
//    TextFormField tfAddress = TextFormField(
//        decoration: InputDecoration(
//          labelText: 'Address*',
//        ),
//        onSaved: (String address) {
//          tenant.address = address;
//        },
//        validator: (address) {
//          if (address.isEmpty) {
//            return "Address can't be empty";
//          }
//        });
//
//    //Occupancy detail textFOrmField
//    TextFormField tfOccupancy = TextFormField(
//        decoration: InputDecoration(
//          labelText: 'Occupancy*',
//          hintText: 'Floor details',
//        ),
//        onSaved: (String occupancy) {
//          tenant.occupancy = occupancy;
//        },
//        validator: (occupancy) {
//          if (occupancy.isEmpty) {
//            return "Occupancy can't be empty";
//          }
//        });
//
//    //email address textFormField
//    TextFormField tfEmail = TextFormField(
//        keyboardType: TextInputType.emailAddress,
//        decoration: InputDecoration(
//          labelText: 'Email Address*',
//        ),
//        onSaved: (String email) {
//          tenant.emailAddress = email;
//        },
//        validator: (email) {
////          if (email.isEmpty) {
////            return "Email Address can't be empty";
////          }
//        });
//
//    //Lease Start date textFormField
//    TextFormField tfLeaseStart = TextFormField(
//        keyboardType: TextInputType.number,
//        decoration: InputDecoration(
//          labelText: 'Lease Start Date*',
//        ),
//        onSaved: (String leaseStart) {
//          tenant.leaseStartDate = leaseStart;
//        },
//        validator: (leaseStart) {
//          if (leaseStart.isEmpty) {
//            return "Lease Start Date can't be empty";
//          }
//        });
//
//    //Lease End date textFormField
//    TextFormField tfLeaseEnd = TextFormField(
//        keyboardType: TextInputType.number,
//        decoration: InputDecoration(
//          labelText: 'Lease End Date*',
//        ),
//        onSaved: (String leaseEnd) {
//          tenant.leaseEndDate = leaseEnd;
//        },
//        validator: (leaseEnd) {
//          if (leaseEnd.isEmpty) {
//            return "Lease End Date can't be empty";
//          }
//        });
//
//    //Rent Paid Date
//    //Rent paid Monthly textFormField
//    TextFormField tfEveryMonth = TextFormField(
//        keyboardType: TextInputType.number,
//        decoration: InputDecoration(
//          hintText: 'Month*',
//        ),
//        onSaved: (String month) {
//          tenant.monthlyValue = month;
//        },
//        validator: (month) {
//          if (month.isEmpty) {
//            return "Month can't be empty";
//          }
//        });
//
//    //Rent paid Day textFormField
//    TextFormField tfEveryDay = TextFormField(
//        keyboardType: TextInputType.number,
//        decoration: InputDecoration(
//          hintText: 'Day*',
//        ),
//        onSaved: (String day) {
//          tenant.dayValue = day;
//        },
//        validator: (day) {
//          if (day.isEmpty) {
//            return "Day can't be empty";
//          }
//        });
//
//    //Payment System
//
//    //type: dropbox
//    DropdownButton dbPaymentType = DropdownButton(
//        items: paymentType.map((String dropDownStringItem) {
//          return DropdownMenuItem<String>(
//            value: dropDownStringItem,
//            child: Text(dropDownStringItem),
//          );
//        }).toList(),
//        //  value: dropDownStringItem,
//        value: 'Monthly',
//        onChanged: (valueSelectedByUser) {
//          setState(() {
//            //   updatePriorityAsInt(valueSelectedByUser);
//            //  debugPrint("show $valueSelectedByUser");
//          });
//        });
//
//    //Monthly Rent textFormField
//    TextFormField tfMonthlyRent = TextFormField(
//        keyboardType: TextInputType.number,
//        decoration: InputDecoration(
//          labelText: 'Monthly Rent*',
//        ),
//        onSaved: (String monthlyRent) {
//          tenant.monthlyRent = monthlyRent;
//        },
//        validator: (monthlyRent) {
//          if (monthlyRent.isEmpty) {
//            return "Monthly Rent can't be empty";
//          }
//        });
//
//    //Electricity textFormField
//    TextFormField tfElectricity = TextFormField(
//        keyboardType: TextInputType.number,
//        decoration: InputDecoration(
//          labelText: 'Electricity*',
//        ),
//        onSaved: (String electricity) {
//          tenant.electricity = electricity;
//        },
//        validator: (electricity) {
//          if (electricity.isEmpty) {
//            return "Electricity can't be empty";
//          }
//        });
//
//    //Water textFormField
//    TextFormField tfWater = TextFormField(
//        keyboardType: TextInputType.number,
//        decoration: InputDecoration(
//          labelText: 'Water*',
//        ),
//        onSaved: (String water) {
//          tenant.water = water;
//        },
//        validator: (water) {
//          if (water.isEmpty) {
//            return "Water can't be empty";
//          }
//        });
//
//    //sanitation textFormField
//    TextFormField tfSanitation = TextFormField(
//        keyboardType: TextInputType.number,
//        decoration: InputDecoration(
//          labelText: 'Sanitation*',
//        ),
//        onSaved: (String sanitation) {
//          tenant.sanitation = sanitation;
//        },
//        validator: (sanitation) {
//          if (sanitation.isEmpty) {
//            return "Sanitation can't be empty";
//          }
//        });
//
//    //Raised Button Cancel
//    RaisedButton rbCancel = RaisedButton(
//        child: Text('Cancel'),
//        color: Colors.green,
//        onPressed: () {
//          Navigator.pop(context);
//        });
//
//    //Raised Save Button
//    RaisedButton rbSave = RaisedButton(
//      child: Text('Save'),
//      color: Colors.green,
//      onPressed: () {
//        if (tenantFormKey.currentState.validate()) {
//          tenantFormKey.currentState.save();
//          this.logic.saveTenant();
//        }
//      },
//    );
//
//    //Raised Update Button
//    RaisedButton rbUpdate = RaisedButton(
//      child: Text('Update'),
//      color: Colors.green,
//      onPressed: () {
//        if (tenantFormKey.currentState.validate()) {
//          tenantFormKey.currentState.save();
//        }
//      },
//    );
//
//    return Form(
//      key: tenantFormKey,
//      child: Padding(
//        padding: EdgeInsets.only(top: 5.0, left: 10.0, right: 10.0),
//        child: ListView(
//          shrinkWrap: true,
//          children: <Widget>[
//            ListTile(
//              leading: const Icon(Icons.contact_phone),
//              title: tfMblNumber,
//              trailing: IconButton(
//                  icon: Icon(Icons.contacts),
//                  onPressed: () {
//                    logic.getContact();
//                  }),
//            ),
//            ListTile(
//              leading: const Icon(Icons.person),
//              title: tffullName,
//            ),
//            ListTile(
//              leading: const Icon(Icons.location_city),
//              title: tfAddress,
//            ),
//            ListTile(
//              leading: Icon(Icons.store),
//              title: tfOccupancy,
//            ),
//            ListTile(
//              leading: Icon(Icons.email),
//              title: tfEmail,
//            ),
//            ListTile(
//              leading: Icon(Icons.date_range),
//              title: tfLeaseStart,
//            ),
//            ListTile(
//              leading: Icon(Icons.date_range),
//              title: tfLeaseEnd,
//            ),
//            Padding(
//              padding: EdgeInsets.only(top: 12.0),
//              child: Text(
//                'How often is Rent Paid?*',
//                style: TextStyle(fontWeight: FontWeight.bold),
//              ),
//            ),
//            Row(
//              children: <Widget>[
//                Expanded(
//                  child: Text('Every'),
//                ),
//                Expanded(
//                  child: tfEveryMonth,
//                ),
//                Expanded(
//                  child: Text('Month(s) On'),
//                ),
//                Expanded(
//                  child: tfEveryDay,
//                ),
//              ],
//            ),
//            Padding(
//              padding: EdgeInsets.only(top: 12.0),
//              child: Text(
//                'Payment System',
//                style: TextStyle(fontWeight: FontWeight.bold),
//              ),
//            ),
//            ListTile(leading: const Icon(Icons.payment), title: dbPaymentType),
//            ListTile(
//              leading: Icon(Icons.receipt),
//              title: tfMonthlyRent,
//            ),
//            ListTile(
//              leading: const Icon(Icons.linear_scale),
//              title: tfElectricity,
//            ),
//            ListTile(
//              leading: const Icon(Icons.branding_watermark),
//              title: tfWater,
//            ),
//            ListTile(
//              leading: const Icon(Icons.border_style),
//              title: tfSanitation,
//            ),
//            Padding(
//              padding: EdgeInsets.only(top: 12.0),
//              child: Row(
//                children: <Widget>[
//                  Visibility(
//                    child: rbCancel,
//                  ),
//                  Container(
//                    margin: EdgeInsets.only(right: 5.0),
//                  ),
//                  Visibility(
//                    visible: !this.isEdit,
//                    child: rbSave,
//                  ),
//                  Visibility(
//                    visible: this.isEdit,
//                    child: rbUpdate,
//                  ),
//                ],
//              ),
//            ),
//          ],
//        ),
//      ),
//    );
//  }
//
  //

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar,
      body: BlocProvider(
        child: Column(
          children: <Widget>[
            Expanded(
              child: NotificationListener<ScrollNotification>(
                onNotification: (scrollNotification) {
                  //   Log.v("scrolled");
                },
                child: SingleChildScrollView(
                  child: getTenantForm(),
                ),
              ),

              //
            ),
          ],
        ),
      ),
    );
  }
}
