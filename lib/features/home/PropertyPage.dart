import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/bloc/blockprovider/BlocProvider.dart';
import 'package:my_rent_tracker/features/bloc/propertyBloc/PropertyBloc.dart';
import 'package:my_rent_tracker/features/home/PropertyPageLogic.dart';
import 'package:my_rent_tracker/features/repo/Property.dart';
import 'package:my_rent_tracker/features/repo/PropertyRepo.dart';
import 'package:my_rent_tracker/utils/Log.dart';

class PropertyPage extends StatefulWidget {
  bool isEdit;
  Property property;

  PropertyPage({this.property, this.isEdit = false});

  @override
  State<StatefulWidget> createState() {
    return PropertyPageState(property: this.property, isEdit: this.isEdit);
  }
}

class PropertyPageState extends State<PropertyPage> {
  bool isEdit;
  Property property;
  AppBar appBar;

  final propertyFormKey = GlobalKey<FormState>();
  PropertyPageLogic logic;
  PropertyBloc propertyBloc;

  PropertyPageState({this.property, this.isEdit = false});

  @override
  void initState() {
    super.initState();
    this.propertyBloc = PropertyBloc(PropertyRepo());
    this.logic = PropertyPageLogic(this, propertyBloc);
    this.logic.onPostState();
  }

  setAppBar(String title) {
    appBar = AppBar(title: Text(title));
  }

  //property form
  Widget propertyForm() {
    Property property = this.logic.property;

    //name textformfield
    TextFormField tfName = TextFormField(
        decoration: InputDecoration(
          labelText: 'Name',
        ),

        onSaved: (String name) {
          property.name = name;
        },
        validator: (name) {
          if (name.isEmpty) {
            return "Name can't be empty";
          }
        });

    //Description textFormField
    TextFormField tfDescription = TextFormField(
        decoration: InputDecoration(
          labelText: 'Description',
        ),
        onSaved: (String description) {
          property.description = description;
        },
        validator: (description) {
          if (description.isEmpty) {
            return "Description can't be empty";
          }
        });

    //image picker from camera
    IconButton ibCamera = IconButton(
        icon: Icon(Icons.camera_alt),
        onPressed: () {
          logic.pickerCamera();
        });

    // image picker from gallery
    IconButton ibGallery = IconButton(
        icon: Icon(Icons.image),
        onPressed: () {
          logic.pickerGallery();
        });

    // Raised Cancel button
    RaisedButton rbCancel = RaisedButton(
        child: Text('Cancel'),
        color: Colors.green,
        onPressed: () {
          moveToLastScreen();
        });

    //Raised Save Button
    RaisedButton rbSave = RaisedButton(
      child: Text('Save'),
      color: Colors.green,
      onPressed: () {
        if (propertyFormKey.currentState.validate()) {
          propertyFormKey.currentState.save();
          this.logic.saveProperty();

          Log.v("Property saved......");
        }
      },
    );

    //Raised Edit Button

    RaisedButton rbEdit = RaisedButton(
      child: Text('Update'),
      color: Colors.green,
      onPressed: () {
        if (propertyFormKey.currentState.validate()) {
          propertyFormKey.currentState.save();
          this.logic.updateProperty();

          Log.v("Property update saved......");
        }
      },
    );

    return Form(
      key: propertyFormKey,
      child: Column(
        children: <Widget>[
          //name textformfield
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(right: 16.0),
              ),
              Expanded(
                flex: 6,
                child: tfName,
              ),
              Expanded(
                flex: 1,
                child: Text(
                  '*',
                  style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontSize: 18.0,
                  ),
                ),
              ),
            ],
          ),

          Row(
            children: [
              Container(
                margin: EdgeInsets.only(right: 16.0),
              ),
              Expanded(
                flex: 6,
                child: tfDescription,
              ),
              Expanded(
                flex: 1,
                child: Text(
                  '*',
                  style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontSize: 18.0,
                  ),
                ),
              ),
            ],
          ),

          //
          Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 16.0),
              ),
              Expanded(
                child: logic.imgFileSelected == null
                    ? Text('No image selected.')
                    : Image.file(logic.imgFileSelected),
              ),
              Expanded(
                child: Row(
                  children: <Widget>[
                    //pick by camera
                    Expanded(
                      child: ibCamera,
                    ),
                    //pick by gallery
                    Expanded(
                      child: ibGallery,
                    ),
                  ],
                ),
              ),
            ],
          ),

          Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 16.0),
              ),
//                Visibility(
//                  child: rbCancel,
//                ),
//                Container(
//                  margin: EdgeInsets.only(right: 2.0),
//                ),

              Expanded(
                flex: 1,
                child: Row(
                  children: <Widget>[
                    Visibility(
                      visible: !this.isEdit,
                      child: rbSave,
                    ),
                    Visibility(
                      visible: this.isEdit,
                      child: rbEdit,

                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: this.appBar,
      body: Padding(
        padding: EdgeInsets.only(left: 0, right: 0),
        child: BlocProvider(
          child: ListView(children: <Widget>[propertyForm()]),
        ),
      ),
    );
  }

  void moveToLastScreen() {
    Navigator.pop(context, true);
  }
}
