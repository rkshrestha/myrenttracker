import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/bloc/tenantBloc/TenantBloc.dart';
import 'package:my_rent_tracker/features/home/TenantPage.dart';
import 'package:my_rent_tracker/features/network/SCResponse.dart';
import 'package:my_rent_tracker/features/repo/Tenant.dart';
import 'package:my_rent_tracker/utils/Log.dart';
import 'package:my_rent_tracker/utils/ToastUtils.dart';
import 'package:contact_picker/contact_picker.dart';

class TenantPageLogic {
  TenantPageState tenantState;
  TenantBloc tenantBloc;
  Tenant tenant = Tenant();

  ContactPicker contactPicker = ContactPicker();
  Contact contact;


  String datePicked;

  TenantPageLogic(this.tenantState, this.tenantBloc);

  void onPostState() {
    if (this.tenantState.isEdit) {
      this.tenantState.setAppBar('Edit Tenant');

      Log.v('inside ' + this.tenantState.isEdit.toString());
    } else {
      this.tenantState.setAppBar('Add Tenant');
      Log.v('inside ' + this.tenantState.isEdit.toString());
    }
  }

  getContact() async {

    Contact contacts = await contactPicker.selectContact();
  contact = contacts ;


  Log.v('phoneNumber: '+ contact.fullName.toString());

    String phoneNumber = contact.phoneNumber.number.toString();

    tenantState.setState((){
      tenantState.mblController = new TextEditingController(text: "$phoneNumber");
    });

  }

  saveTenant() async {
    this.tenantBloc.saveTenant(this.tenant).listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        ToastUtils.show(it.data);
        Navigator.pop(this.tenantState.context);
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(tenantState.context).pop();
        });
      }
    });
  }

//getDate(){
//  NepaliDatePicker.showPicker(
//      context: tenantState.context,
//      startYear: 2052,
//      endYear: 2100,
//      color: Colors.green,
//      barrierDismissible: false,
//      onPicked: (DateTime date) {
//        tenantState.setState(() {
//          ///Iso8601String Format: 2018-12-23T00:00:00
////          dateController.text =
////              date.toIso8601String().split("T").first;
//          datePicked= date.toIso8601String().split("T").first;
//
//          Log.v('date:' +datePicked);
//
//        });
//      });
//}

}
