import 'package:flutter/material.dart';
import 'package:my_rent_tracker/features/bloc/blockprovider/BlocProvider.dart';
import 'package:my_rent_tracker/features/bloc/tenantBloc/TenantBloc.dart';
import 'package:my_rent_tracker/features/home/TenantPageListLogic.dart';
import 'package:my_rent_tracker/features/home/TransactionPageList.dart';
import 'package:my_rent_tracker/features/repo/Tenant.dart';
import 'package:my_rent_tracker/features/repo/TenantRepo.dart';

class TenantPageList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TenantPageListState();
  }
}

class TenantPageListState extends State<TenantPageList> {
  TenantPageListLogic logic;
  TenantBloc tenantBloc;
  Tenant tenant;

  @override
  void initState() {
    super.initState();

    tenantBloc = TenantBloc(TenantRepo());
    logic = TenantPageListLogic(this, tenantBloc);
    this.logic.onPostInit();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tenant of #property'),
      ),
      body: BlocProvider(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 6.0),
              child: Text(
                "Tenant List",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15.0,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Expanded(
              child: getTenantList(),
            ),
          ],
        ),
      ),
      floatingActionButton: Padding(
        padding: EdgeInsets.only(bottom: 35.0),
        child: FloatingActionButton(
            child: Icon(Icons.add),
            tooltip: 'Add Tenant',
            backgroundColor: Colors.green,
            onPressed: () {
              logic.onAddTenant();
            }),
      ),
    );
  }

  ListView getTenantList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: this.logic.tenantList.length,
        itemBuilder: (BuildContext context, int position) {
          Tenant tenant = this.logic.tenantList[position];

          return Dismissible(
            key: Key(this.logic.tenantList[position].tenantId.toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
              Tenant tenants = this.logic.tenantList[position];

              this.logic.confirmationDeleteTenant(tenants, position);
              // this.logic.deleteTenant(tenant, position);
            },
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ),
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                title: Text(tenant.fullName),
                subtitle: Text(tenant.mobileNumber),
                trailing: GestureDetector(
                  child: Icon(
                    Icons.description,
                    size: 30.0,
                    color: Colors.black,
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => TransactionListPage()));
                  },
                ),
                onTap: () {
                  logic.onEditTenant(tenant, position);
                },
              ),
            ),
          );
        });
  }
}
