import 'package:flutter/material.dart';

class InfoPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _InfoPageState();
  }
}

class _InfoPageState extends State<InfoPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        /*  leading: Icon(
          Icons.home,
          size: 28.0,
        ),*/
        title: Text("Info"),
      ),
      body: new Container(
        child: Column(
          children: <Widget>[
            new Expanded(
              child: Text(
                'Quick Guide',
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
