import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:chopper/chopper.dart';
import 'package:my_rent_tracker/features/network/NetworkApi.dart';
import 'package:my_rent_tracker/features/network/SCResponse.dart';
import 'package:my_rent_tracker/features/repo/JsonCallable.dart';
import 'package:my_rent_tracker/utils/DsonUtils.dart';
import 'package:my_rent_tracker/utils/Log.dart';


class NetworkClient {
  ChopperClient client(
      {String newBaseUrl,
        bool useFormUrlEncodedConverter = false
      }) {
    ChopperClient chopper = ChopperClient(
      baseUrl: (newBaseUrl != null)
          ? newBaseUrl
          : "http://kisanapi.pathway.com.np/api/v1/pef",
      services: [
        // the generated service
        NetworkApi.create()
      ],
      converter: (useFormUrlEncodedConverter)
          ? FormUrlEncodedConverter()
          : JsonConverter(),
      interceptors: [
            (request) async => applyHeader(request, "Authorization", ""),
        AuthInterceptor(),
        LogInterceptor(),
      ],
    );
    return chopper;
  }

  Future<SCResponse<T>> call<T>(Future<Response<dynamic>> future,JsonCallable callable) async {

    try {
      Response<dynamic> response= await future;
      dynamic mappedResponse=useDecoder<T>(response.body, callable);
      Log.v("mapping actual server response");
      Log.v(jsonEncode(mappedResponse));
      Log.v(
          "=================================================================================================");
      return SCResponse.success(mappedResponse);
    }catch(e){
     // Log.v(e.toString());
      String errorMessage="";

      if(e is SocketException){
        errorMessage = "Unable to connect with server!";
        return SCResponse.error(errorMessage);
      }

      if(e is TimeoutException){
        errorMessage = "Timeout!";
        return SCResponse.error(errorMessage);
      }

      if(e is Response){
        errorMessage=e.body;
        if(e.statusCode == 403) {
          return SCResponse.tokenExpired(errorMessage);
        }else{
          Map data=jsonDecode(errorMessage);
          return SCResponse.error(data['Message']);
        }

      }

      return SCResponse.error("Server Error!");

    }
  }


    dynamic useDecoder<T>(entity,JsonCallable callable) {
      if (entity is String) {
        Log.v("entity is string");
        return entity;
      }

      if (callable == null) {
        Log.v(
            "Warning ! Callable required!!You may have forgot to pass the callable in your network client");
        Log.v(
            "Warning ! Couldn't map server response to your callable?");

        throw Exception("Parsing Error");
      }

      if (entity is Iterable) {
        return DsonUtils.dynamicToList<T>(entity, callable);
      }

      if (entity is Map) {
        String data = jsonEncode(entity);
        return jsonDecode(data) as Map<String, dynamic>;
      }

      return DsonUtils.toObject(entity, callable);
    }

}



class AuthInterceptor implements RequestInterceptor {
  @override
  FutureOr<Request> onRequest(Request request) {
    Log.v("inside auth interceptor");
    request.headers.update("Authorization", (value) => "update value");
    request.headers.forEach((key, value) => Log.v(key + "," + value));
    Log.v("inside autha interceptor");
    return request;
  }
}

class LogInterceptor extends HttpLoggingInterceptor {
  @override
  FutureOr<Request> onRequest(Request request) async {
    setShowReminder();
    Log.v("request url : " + jsonEncode(request.baseUrl + request.url));
    // return  super.onRequest(request);
    return request;
  }

  void setShowReminder() {
    // if (this.showReminder) {
    Log.v(
        "=================================================================================================");
    Log.v(
        'Tips : "Please use following command if you have update your api or changes on api file"');
    Log.v(
        "> flutter packages pub run build_runner build --delete-conflicting-outputs or you may use watcher cmd!");
    Log.v("> flutter packages pub run build_runner watch");
    Log.v('"This will reflect changes of your api"');
    Log.v(
        "=================================================================================================");
    //  }
  }

  @override
  FutureOr<Response> onResponse(Response response) {
    // Log.v("=================================================================================================");
    //Log.v("logging response from server... ");
    Log.v("status code :  " + response.statusCode.toString());
    Log.v("actual server response : " + jsonEncode(response.body));
    Log.v(
        "=================================================================================================");
    return response;
  }
}

//class SCFormUrlEncodedConverter extends FormUrlEncodedConverter {
//
//  JsonCallable callable;
//
//  SCFormUrlEncodedConverter(this.callable);
//
//  dynamic _decode<T>(entity) {
//    if (entity is String) {
//      Log.v("entity is string");
//      return entity;
//    }
//
//    if (entity is Iterable) {
//      return DsonUtils.dynamicToList(entity, this.callable);
//    }
//
//    if (entity is Map) {
//      String data = jsonEncode(entity);
//      return jsonDecode(data) as Map<String, dynamic>;
//    }
//
//    return DsonUtils.toObject(entity, this.callable);
//  }
//}

//class SCJsonConveter extends JsonConverter {
//  JsonCallable callable;
//
//  SCJsonConveter(this.callable);
//
//  dynamic _decode<T>(entity) {
//    if (entity is String) {
//      Log.v("entity is string");
//      return entity;
//    }
//
//    if (this.callable == null) {
//      Log.v(
//          "Warning ! Callable required!!You may have forgot to pass the callable in your network client");
//      Log.v(
//          "Warning ! Couldn't map server response to your callable?");
//    }
//
//    if (entity is Iterable) {
//      return DsonUtils.dynamicToList(entity, this.callable);
//    }
//
//    if (entity is Map) {
//      String data = jsonEncode(entity);
//      return jsonDecode(data) as Map<String, dynamic>;
//    }
//
//    return DsonUtils.toObject(entity, this.callable);
//  }
//
//  @override
//  Response convertResponse<T>(Response response) {
//    // use [JsonConverter] to decode json
//    final jsonRes = super.convertResponse<T>(response);
//    Log.v(
//        "=================================================================================================");
//    Log.v("actual server response : " + jsonEncode(jsonRes.body));
//    Log.v(
//        "=================================================================================================");
//
//    return jsonRes.replace<T>(
//      body: _decode<T>(jsonRes.body),
//    );
//  }
//
//  @override
//  // all objects should implements toJson method
//  Request convertRequest(Request request) => super.convertRequest(request);
//}