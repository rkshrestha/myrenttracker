// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'NetworkApi.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

class _$NetworkApi extends NetworkApi {
  _$NetworkApi([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  final definitionType = NetworkApi;

  Future<Response> getResource(String id) {
    final $url = '/atfbyorganisation/$id';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> getSalesList(String id) {
    final $url = '/coop/shg-network/$id/sales-list';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }
}
