//import 'package:flutter_app/features/repo/User.dart';
//import 'package:jaguar_retrofit/jaguar_retrofit.dart';
//import 'package:jaguar_resty/jaguar_resty.dart' as resty;
//import 'package:jaguar_resty/jaguar_resty.dart';
//part 'JaguarApi.jretro.dart';
//
//@GenApiClient()
//class JaguarApi extends ApiClient with _$JaguarApiClient {
//
//  final resty.Route base;
//
//  JaguarApi(this.base);
//
//  @GetReq(path: "atfbyorganisation/:atfId")
//  Future<User> getUserById(@PathParam() int atfId);
//
//
//}



import "dart:async";
import 'package:chopper/chopper.dart';
part 'NetworkApi.chopper.dart';
// https://javiercbk.github.io/json_to_dart/
@ChopperApi()
abstract class NetworkApi extends ChopperService {

  static NetworkApi create([ChopperClient client]) => _$NetworkApi(client);

  @Get(path: "atfbyorganisation/{id}")
  Future<Response> getResource(@Path() String id);


  @Get(path: "coop/shg-network/{id}/sales-list")
  Future<Response> getSalesList(@Path() String id);

}