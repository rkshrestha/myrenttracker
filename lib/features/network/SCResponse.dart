class SCResponse<T> {
  Status status;
  T data;
  String errorMessage;

  SCResponse(this.status, this.data, this.errorMessage);

  static SCResponse<T> success<T>(T data) {
    return SCResponse(Status.SUCCESS, data, null);
  }

  static SCResponse<T> error<T>(String error) {
    return SCResponse(Status.ERROR, null, error);
  }

  static SCResponse<T> tokenExpired<T>(String error) {
    return SCResponse(Status.TOKEN_EXPIRED, null, error);
  }

  static SCResponse<T> loading<T>() {
    return SCResponse(Status.LOADING, null, null);
  }

  static SCResponse<T> loadingFinished<T>() {
    return SCResponse(Status.LOADING_FINISHED, null, null);
  }

  static SCResponse<T> noInternetAvailable<T>() {
    return SCResponse(
        Status.NO_INTERNET_AVAILABLE, null, "No internet connection found");
  }
}

enum Status {
  LOADING,
  LOADING_FINISHED,
  SUCCESS,
  ERROR,
  NO_INTERNET_AVAILABLE,
  TOKEN_EXPIRED
}
